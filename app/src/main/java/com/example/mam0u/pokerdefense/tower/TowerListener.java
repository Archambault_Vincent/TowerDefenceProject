package com.example.mam0u.pokerdefense.tower;

public interface TowerListener {
    void damageInflicted(float totalDamage);
    void valueChanged(int value);
}
