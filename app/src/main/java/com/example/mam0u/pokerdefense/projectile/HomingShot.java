package com.example.mam0u.pokerdefense.projectile;


import com.example.mam0u.pokerdefense.enemy.Enemy;
import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.entity.Entity;
import com.example.mam0u.pokerdefense.entity.EntityListener;

public abstract class HomingShot extends Shot implements EntityListener {

    private Enemy mTarget;
    private boolean mTargetReached;

    HomingShot(Entity origin) {
        super(origin);
    }

    @Override
    public void clean() {
        super.clean();
        setTarget(null);
    }

    @Override
    public void tick() {
        super.tick();

        if (isEnabled() && mTarget != null && getDistanceTo(mTarget) <= getSpeed() / GameEngine.TARGET_FRAME_RATE) {
            mTargetReached = true;
            targetReached();
        }
    }

    public Enemy getTarget() {
        return mTarget;
    }

    public void setTarget(Enemy target) {
        if (mTarget != null) {
            mTarget.removeListener(this);
        }

        mTarget = target;
        mTargetReached = false;

        if (mTarget != null) {
            mTarget.addListener(this);
        }
    }

    protected abstract void targetReached();

    protected abstract void targetLost();

    @Override
    public void entityRemoved(Entity entity) {
        if (!mTargetReached) {
            setTarget(null);
            targetLost();
        }
    }

}
