package com.example.mam0u.pokerdefense.plateau;

import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.entity.Entity;
import com.example.mam0u.pokerdefense.entity.Types;
import com.example.mam0u.pokerdefense.iterator.Predicate;

public abstract class Plateau extends Entity {
    Plateau(GameEngine gameEngine) {
        super(gameEngine);
    }

    public static Predicate<Plateau> unoccupied() {
        return new Predicate<Plateau>() {
            @Override
            public boolean apply(Plateau value) {
                return !value.isOccupied();
            }
        };
    }

    private boolean mOccupied;

    @Override
    public final int getEntityType() {
        return Types.PLATEAU;
    }

    public boolean isOccupied() {
        return mOccupied;
    }

    public void setOccupied(boolean occupied) {
        mOccupied = occupied;
    }
}
