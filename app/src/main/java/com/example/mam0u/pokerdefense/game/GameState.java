package com.example.mam0u.pokerdefense.game;

import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.loop.Message;
import com.example.mam0u.pokerdefense.theme.Theme;
import com.example.mam0u.pokerdefense.theme.ThemeListener;
import com.example.mam0u.pokerdefense.theme.ThemeManager;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class GameState implements ThemeListener, LivesListener {
    private final static String TAG = GameState.class.getSimpleName();

    private final GameEngine mGameEngine;
    private final ScoreBoard mScoreBoard;

    private boolean mGameOver = false;
    private boolean mGameStarted = false;

    private List<GameStateListener> mListeners = new CopyOnWriteArrayList<>();

    public GameState(GameEngine gameEngine, ThemeManager themeManager, ScoreBoard scoreBoard) {
        mGameEngine = gameEngine;
        mScoreBoard = scoreBoard;

        mScoreBoard.addLivesListener(this);
        themeManager.addListener(this);
    }

    public void restart() {
        if (mGameEngine.isThreadChangeNeeded()) {
            mGameEngine.post(new Message() {
                @Override
                public void execute() {
                    restart();
                }
            });
            return;
        }

        for (GameStateListener listener : mListeners) {
            listener.gameRestart();
        }

        mGameOver = false;
        mGameStarted = false;
    }

    public boolean isGameOver() {
        return mGameOver;
    }

    public boolean isGameStarted() {
        return !mGameOver && mGameStarted;
    }

    public void addListener(GameStateListener listener) {
        mListeners.add(listener);
    }

    public void removeListener(GameStateListener listener) {
        mListeners.remove(listener);
    }

    @Override
    public void livesChanged(int lives) {
        if (!mGameOver && mScoreBoard.getLives() < 0) {
            mGameOver = true;

            for (GameStateListener listener : mListeners) {
                listener.gameOver();
            }
        }
    }

    @Override
    public void themeChanged(Theme theme) {
        restart();
    }

    public void setGameStarted() {
        mGameStarted = true;
    }
}
