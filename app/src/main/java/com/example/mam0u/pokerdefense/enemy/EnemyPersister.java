package com.example.mam0u.pokerdefense.enemy;


import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.entity.Entity;
import com.example.mam0u.pokerdefense.entity.EntityRegistry;
import com.example.mam0u.pokerdefense.game.EnemyDescriptor;
import com.example.mam0u.pokerdefense.game.EntityDescriptor;
import com.example.mam0u.pokerdefense.persist.EntityPersister;

public class EnemyPersister extends EntityPersister {

    public EnemyPersister(GameEngine gameEngine, EntityRegistry entityRegistry, String entityName) {
        super(gameEngine, entityRegistry, entityName);
    }

    @Override
    protected EnemyDescriptor createEntityDescriptor() {
        return new EnemyDescriptor();
    }

    @Override
    protected EnemyDescriptor writeEntityDescriptor(Entity entity) {
        Enemy enemy = (Enemy) entity;
        EnemyDescriptor enemyDescriptor = (EnemyDescriptor) super.writeEntityDescriptor(entity);

        enemyDescriptor.setId(enemy.getEntityId());
        enemyDescriptor.setName(enemy.getEntityName());
        enemyDescriptor.setPosition(enemy.getPosition());
        enemyDescriptor.setHealth(enemy.getHealth());
        enemyDescriptor.setMaxHealth(enemy.getMaxHealth());
        enemyDescriptor.setWayPoints(enemy.getWayPoints());
        enemyDescriptor.setWayPointIndex(enemy.getWayPointIndex());
        enemyDescriptor.setWaveNumber(enemy.getWaveNumber());
        enemyDescriptor.setReward(enemy.getReward());

        return enemyDescriptor;
    }

    @Override
    protected Enemy readEntityDescriptor(EntityDescriptor entityDescriptor) {
        Enemy enemy = (Enemy) super.readEntityDescriptor(entityDescriptor);
        EnemyDescriptor enemyDescriptor = (EnemyDescriptor) entityDescriptor;

        enemy.setHealth(enemyDescriptor.getHealth(), enemyDescriptor.getMaxHealth());
        enemy.setReward(enemyDescriptor.getReward());
        enemy.setPosition(enemyDescriptor.getPosition());
        enemy.setWaveNumber(enemyDescriptor.getWaveNumber());
        enemy.setupPath(enemyDescriptor.getWayPoints(), enemyDescriptor.getWayPointIndex());

        return enemy;
    }

}
