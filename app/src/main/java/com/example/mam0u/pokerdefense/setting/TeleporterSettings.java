package com.example.mam0u.pokerdefense.setting;

import com.example.mam0u.pokerdefense.tower.TowerSettings;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
public class TeleporterSettings extends TowerSettings {

    @Element(name = "teleportDistance")
    private float mTeleportDistance;

    @Element(name = "enhanceTeleportDistance")
    private float mEnhanceTeleportDistance;

    public float getTeleportDistance() {
        return mTeleportDistance;
    }

    public float getEnhanceTeleportDistance() {
        return mEnhanceTeleportDistance;
    }
}
