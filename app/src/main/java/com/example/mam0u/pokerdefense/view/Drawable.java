package com.example.mam0u.pokerdefense.view;

import android.graphics.Canvas;

public interface Drawable {
    int getLayer();
    void draw(Canvas canvas);
}
