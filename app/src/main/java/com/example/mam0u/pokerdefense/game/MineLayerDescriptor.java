package com.example.mam0u.pokerdefense.game;

import com.example.mam0u.pokerdefense.math.Vector2;

import org.simpleframework.xml.ElementList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;



public class MineLayerDescriptor extends TowerDescriptor {

    @ElementList(name = "minePositions", entry = "position")
    Collection<Vector2> mMinePositions = new ArrayList<>();

    public Collection<Vector2> getMinePositions() {
        return Collections.unmodifiableCollection(mMinePositions);
    }

    public void setMinePositions(Collection<Vector2> minePositions) {
        mMinePositions = minePositions;
    }
}
