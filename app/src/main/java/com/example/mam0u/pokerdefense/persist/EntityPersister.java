package com.example.mam0u.pokerdefense.persist;


import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.entity.Entity;
import com.example.mam0u.pokerdefense.entity.EntityRegistry;
import com.example.mam0u.pokerdefense.game.EntityDescriptor;
import com.example.mam0u.pokerdefense.game.GameDescriptorRoot;
import com.example.mam0u.pokerdefense.iterator.StreamIterator;

public class EntityPersister implements Persister {

    private final GameEngine mGameEngine;
    private final EntityRegistry mEntityRegistry;
    private final String mEntityName;

    public EntityPersister(GameEngine gameEngine, EntityRegistry entityRegistry, String entityName) {
        mGameEngine = gameEngine;
        mEntityRegistry = entityRegistry;
        mEntityName = entityName;
    }

    @Override
    public void writeDescriptor(GameDescriptorRoot gameDescriptor) {
        StreamIterator<Entity> iterator = mGameEngine.getAllEntities()
                .filter(Entity.nameEquals(mEntityName));

        while (iterator.hasNext()) {
            Entity entity = iterator.next();
            gameDescriptor.addEntityDescriptor(writeEntityDescriptor(entity));
        }
    }

    @Override
    public void readDescriptor(GameDescriptorRoot gameDescriptor) {
        for (EntityDescriptor entityDescriptor : gameDescriptor.getEntityDescriptors()) {
            if (mEntityName.equals(entityDescriptor.getName())) {
                mGameEngine.add(readEntityDescriptor(entityDescriptor));
            }
        }
    }

    protected EntityDescriptor createEntityDescriptor() {
        return new EntityDescriptor();
    }

    protected EntityDescriptor writeEntityDescriptor(Entity entity) {
        EntityDescriptor entityDescriptor = createEntityDescriptor();

        entityDescriptor.setId(entity.getEntityId());
        entityDescriptor.setName(entity.getEntityName());

        return entityDescriptor;
    }

    protected Entity readEntityDescriptor(EntityDescriptor entityDescriptor) {
        return mEntityRegistry.createEntity(entityDescriptor.getName(), entityDescriptor.getId());
    }

    protected GameEngine getGameEngine() {
        return mGameEngine;
    }

    protected EntityRegistry getEntityRegistry() {
        return mEntityRegistry;
    }

}
