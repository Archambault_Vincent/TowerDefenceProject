package com.example.mam0u.pokerdefense.setting;

public enum BackButtonMode {
    DISABLED,
    ENABLED,
    TWICE
}
