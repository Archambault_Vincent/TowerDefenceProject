package com.example.mam0u.pokerdefense.game;

import android.content.Context;

import com.example.mam0u.pokerdefense.R;
import com.example.mam0u.pokerdefense.engine.GameConfiguration;
import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.entity.EntityRegistry;
import com.example.mam0u.pokerdefense.loop.Message;
import com.example.mam0u.pokerdefense.map.MapDescriptorRoot;
import com.example.mam0u.pokerdefense.map.MapInfo;
import com.example.mam0u.pokerdefense.map.MapRepository;
import com.example.mam0u.pokerdefense.map.PlateauDescriptor;
import com.example.mam0u.pokerdefense.persist.Persister;
import com.example.mam0u.pokerdefense.plateau.Plateau;
import com.example.mam0u.pokerdefense.setting.EnemySettingsRoot;
import com.example.mam0u.pokerdefense.setting.GameSettingsRoot;
import com.example.mam0u.pokerdefense.setting.TowerSettingsRoot;
import com.example.mam0u.pokerdefense.view.Viewport;
import com.example.mam0u.pokerdefense.wave.WaveDescriptorRoot;


public class GameConfigurationLoader implements Persister, GameStateListener {
    private final Context mContext;
    private final GameEngine mGameEngine;
    private final Viewport mViewport;
    private final ScoreBoard mScoreBoard;
    private final EntityRegistry mEntityRegistry;
    private final GameState mGameState;
    private final MapRepository mMapRepository;

    private MapInfo mMapInfo;

    public GameConfigurationLoader(Context context, GameEngine gameEngine, ScoreBoard scoreBoard,
                                   GameState gameState, Viewport viewport,
                                   EntityRegistry entityRegistry, MapRepository mapRepository) {
        mContext = context;
        mGameEngine = gameEngine;
        mViewport = viewport;
        mScoreBoard = scoreBoard;
        mEntityRegistry = entityRegistry;
        mGameState = gameState;
        mMapRepository = mapRepository;

        mGameState.addListener(this);

        setGameConfiguration(mMapRepository.getMapInfos().get(0));
    }

    public MapInfo getMapInfo() {
        return mMapInfo;
    }

    public void loadMap(final MapInfo mapInfo) {
        if (mGameEngine.isThreadChangeNeeded()) {
            mGameEngine.post(new Message() {
                @Override
                public void execute() {
                    loadMap(mapInfo);
                }
            });
            return;
        }

        if (mMapInfo == mapInfo) {
            return;
        }

        setGameConfiguration(mapInfo);
        mGameState.restart();
    }

    private void setGameConfiguration(MapInfo mapInfo) {
        mMapInfo = mapInfo;

        try {
            mGameEngine.setGameConfiguration(new GameConfiguration(
                    GameSettingsRoot.fromXml(mContext, R.raw.game_settings),
                    EnemySettingsRoot.fromXml(mContext, R.raw.enemy_settings),
                    TowerSettingsRoot.fromXml(mContext, R.raw.tower_settings),
                    MapDescriptorRoot.fromXml(mContext, mapInfo.getMapDescriptorResId()),
                    WaveDescriptorRoot.fromXml(mContext, R.raw.wave_descriptors)
            ));
        } catch (Exception e) {
            throw new RuntimeException("Could not load map!", e);
        }
    }

    @Override
    public void gameRestart() {
        mGameEngine.clear();

        GameConfiguration configuration = mGameEngine.getGameConfiguration();

        for (PlateauDescriptor descriptor : configuration.getMapDescriptorRoot().getPlateaus()) {
            Plateau plateau = (Plateau) mEntityRegistry.createEntity(descriptor.getName());
            plateau.setPosition(descriptor.getPosition());
            mGameEngine.add(plateau);
        }

        mViewport.setGameSize(configuration.getMapDescriptorRoot().getWidth(), configuration.getMapDescriptorRoot().getHeight());
        mScoreBoard.reset(configuration.getGameSettingsRoot().getLives(), configuration.getGameSettingsRoot().getCredits());
    }

    @Override
    public void gameOver() {

    }

    @Override
    public void writeDescriptor(GameDescriptorRoot gameDescriptor) {
        gameDescriptor.setMapId(mMapInfo.getMapId());
    }

    @Override
    public void readDescriptor(GameDescriptorRoot gameDescriptor) {
        setGameConfiguration(mMapRepository.getMapById(gameDescriptor.getMapId()));
        GameConfiguration configuration = mGameEngine.getGameConfiguration();
        mViewport.setGameSize(configuration.getMapDescriptorRoot().getWidth(), configuration.getMapDescriptorRoot().getHeight());
    }
}
