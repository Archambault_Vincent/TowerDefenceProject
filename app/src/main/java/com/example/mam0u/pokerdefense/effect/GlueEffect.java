package com.example.mam0u.pokerdefense.effect;

import android.graphics.Paint;

import com.example.mam0u.pokerdefense.R;
import com.example.mam0u.pokerdefense.enemy.Enemy;
import com.example.mam0u.pokerdefense.enemy.Flyer;
import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.entity.Entity;
import com.example.mam0u.pokerdefense.math.RandomUtils;
import com.example.mam0u.pokerdefense.math.Vector2;
import com.example.mam0u.pokerdefense.sprite.SpriteInstance;
import com.example.mam0u.pokerdefense.sprite.SpriteTemplate;
import com.example.mam0u.pokerdefense.sprite.SpriteTransformation;
import com.example.mam0u.pokerdefense.sprite.SpriteTransformer;
import com.example.mam0u.pokerdefense.sprite.StaticSprite;
import com.example.mam0u.pokerdefense.view.Layers;


public class GlueEffect extends AreaEffect implements SpriteTransformation {

    private final static int ALPHA_START = 150;

    private class StaticData {
        SpriteTemplate mSpriteTemplate;
    }

    private float mAngle;
    private float mIntensity;
    private int mAlphaStep;

    private Paint mPaint;
    private StaticSprite mSprite;

    public GlueEffect(Entity origin, Vector2 position, float intensity, float duration) {
        super(origin, duration, 1f);
        setPosition(position);

        mIntensity = intensity;
        mAngle = RandomUtils.next(360f);
        mAlphaStep = (int) (ALPHA_START / (GameEngine.TARGET_FRAME_RATE * duration));

        StaticData s = (StaticData) getStaticData();

        mSprite = getSpriteFactory().createStatic(Layers.BOTTOM, s.mSpriteTemplate);
        mSprite.setListener(this);
        mSprite.setIndex(RandomUtils.next(4));

        mPaint = new Paint();
        mPaint.setAlpha(ALPHA_START);
        mSprite.setPaint(mPaint);
    }

    @Override
    public Object initStatic() {
        StaticData s = new StaticData();

        s.mSpriteTemplate = getSpriteFactory().createTemplate(R.attr.glueEffect, 4);
        s.mSpriteTemplate.setMatrix(1f, 1f, null, null);

        return s;
    }

    @Override
    public void init() {
        super.init();

        getGameEngine().add(mSprite);
    }

    @Override
    public void clean() {
        super.clean();

        getGameEngine().remove(mSprite);
    }

    @Override
    public void draw(SpriteInstance sprite, SpriteTransformer transformer) {
        transformer.translate(getPosition());
        transformer.rotate(mAngle);
    }

    @Override
    public void tick() {
        super.tick();

        mPaint.setAlpha(mPaint.getAlpha() - mAlphaStep);
    }

    @Override
    protected void enemyEnter(Enemy e) {
        if (!(e instanceof Flyer)) {
            e.modifySpeed(1f / mIntensity);
        }
    }

    @Override
    protected void enemyExit(Enemy e) {
        if (!(e instanceof Flyer)) {
            e.modifySpeed(mIntensity);
        }
    }
}
