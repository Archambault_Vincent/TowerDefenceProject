package com.example.mam0u.pokerdefense.poker;

public class Card {
    private boolean change = false;
    private  String color ;
    private int type ;
    private int image;
    public Card(String color, int type, int image){
        this.image = image;
        this.setColor(color) ;
        this.setType(type);
    }
    public void setType(int type) {
        this.type = type;
    }
    public int getType() {
        return type;
    }
    public void setColor(String color) {this.color = color;	}
    public String getColor() {
        return color;
    }
    public void setChange(boolean change) {
        this.change = change;
    }
    public boolean isChange() {
        return change;
    }
    public int getImage() {return image;}
    public void setImage(int image) {this.image = image;}
}
