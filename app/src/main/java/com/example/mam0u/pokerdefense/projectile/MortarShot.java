package com.example.mam0u.pokerdefense.projectile;


import com.example.mam0u.pokerdefense.R;
import com.example.mam0u.pokerdefense.effect.Explosion;
import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.entity.Entity;
import com.example.mam0u.pokerdefense.math.Function;
import com.example.mam0u.pokerdefense.math.RandomUtils;
import com.example.mam0u.pokerdefense.math.SampledFunction;
import com.example.mam0u.pokerdefense.math.Vector2;
import com.example.mam0u.pokerdefense.sprite.SpriteInstance;
import com.example.mam0u.pokerdefense.sprite.SpriteTemplate;
import com.example.mam0u.pokerdefense.sprite.SpriteTransformation;
import com.example.mam0u.pokerdefense.sprite.SpriteTransformer;
import com.example.mam0u.pokerdefense.sprite.StaticSprite;
import com.example.mam0u.pokerdefense.view.Layers;

public class MortarShot extends Shot implements SpriteTransformation {

    public final static float TIME_TO_TARGET = 1.5f;
    private final static float HEIGHT_SCALING_START = 0.5f;
    private final static float HEIGHT_SCALING_STOP = 1.0f;
    private final static float HEIGHT_SCALING_PEAK = 1.5f;

    private class StaticData {
        SpriteTemplate mSpriteTemplate;
    }

    private float mDamage;
    private float mRadius;
    private float mAngle;
    private SampledFunction mHeightScalingFunction;

    private StaticSprite mSprite;

    public MortarShot(Entity origin, Vector2 position, Vector2 target, float damage, float radius) {
        super(origin);
        setPosition(position);
        setSpeed(getDistanceTo(target) / TIME_TO_TARGET);
        setDirection(getDirectionTo(target));

        mDamage = damage;
        mRadius = radius;
        mAngle = RandomUtils.next(360f);

        StaticData s = (StaticData) getStaticData();

        float x1 = (float) Math.sqrt(HEIGHT_SCALING_PEAK - HEIGHT_SCALING_START);
        float x2 = (float) Math.sqrt(HEIGHT_SCALING_PEAK - HEIGHT_SCALING_STOP);
        mHeightScalingFunction = Function.quadratic()
                .multiply(-1f)
                .offset(HEIGHT_SCALING_PEAK)
                .shift(-x1)
                .stretch(GameEngine.TARGET_FRAME_RATE * TIME_TO_TARGET / (x1 + x2))
                .sample();

        mSprite = getSpriteFactory().createStatic(Layers.SHOT, s.mSpriteTemplate);
        mSprite.setListener(this);
        mSprite.setIndex(RandomUtils.next(4));
    }

    @Override
    public Object initStatic() {
        StaticData s = new StaticData();

        s.mSpriteTemplate = getSpriteFactory().createTemplate(R.attr.grenade, 4);
        s.mSpriteTemplate.setMatrix(0.7f, 0.7f, null, null);

        return s;
    }

    @Override
    public void init() {
        super.init();

        getGameEngine().add(mSprite);
    }

    @Override
    public void clean() {
        super.clean();

        getGameEngine().remove(mSprite);
    }

    @Override
    public void draw(SpriteInstance sprite, SpriteTransformer transformer) {
        float s = mHeightScalingFunction.getValue();
        transformer.translate(getPosition());
        transformer.scale(s);
        transformer.rotate(mAngle);
    }

    @Override
    public void tick() {
        super.tick();

        mHeightScalingFunction.step();
        if (mHeightScalingFunction.getPosition() >= GameEngine.TARGET_FRAME_RATE * TIME_TO_TARGET) {
            getGameEngine().add(new Explosion(getOrigin(), getPosition(), mDamage, mRadius));
            this.remove();
        }
    }
}
