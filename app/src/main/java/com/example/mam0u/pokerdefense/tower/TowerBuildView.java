package com.example.mam0u.pokerdefense.tower;

public interface TowerBuildView {
    void showTowerBuildView();
    void hideTowerBuildView();
}
