package com.example.mam0u.pokerdefense.iterator;

public interface StreamIterable<T> extends Iterable<T> {
    StreamIterator<T> iterator();
}
