package com.example.mam0u.pokerdefense.sprite;

public interface SpriteTransformation {
    void draw(SpriteInstance sprite, SpriteTransformer transformer);
}
