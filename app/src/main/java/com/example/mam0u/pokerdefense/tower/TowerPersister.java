package com.example.mam0u.pokerdefense.tower;


import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.entity.Entity;
import com.example.mam0u.pokerdefense.entity.EntityRegistry;
import com.example.mam0u.pokerdefense.game.EntityDescriptor;
import com.example.mam0u.pokerdefense.game.TowerDescriptor;
import com.example.mam0u.pokerdefense.persist.EntityPersister;
import com.example.mam0u.pokerdefense.plateau.Plateau;

public class TowerPersister extends EntityPersister {

    public TowerPersister(GameEngine gameEngine, EntityRegistry entityRegistry, String entityName) {
        super(gameEngine, entityRegistry, entityName);
    }

    @Override
    protected TowerDescriptor createEntityDescriptor() {
        return new TowerDescriptor();
    }

    @Override
    protected TowerDescriptor writeEntityDescriptor(Entity entity) {
        Tower tower = (Tower) entity;
        TowerDescriptor towerDescriptor = (TowerDescriptor) super.writeEntityDescriptor(tower);

        towerDescriptor.setId(tower.getEntityId());
        towerDescriptor.setName(tower.getEntityName());
        towerDescriptor.setPosition(tower.getPosition());
        towerDescriptor.setValue(tower.getValue());
        towerDescriptor.setLevel(tower.getLevel());
        towerDescriptor.setDamageInflicted(tower.getDamageInflicted());

        return towerDescriptor;
    }

    @Override
    protected Tower readEntityDescriptor(EntityDescriptor entityDescriptor) {
        Tower tower = (Tower) super.readEntityDescriptor(entityDescriptor);
        TowerDescriptor towerDescriptor = (TowerDescriptor) entityDescriptor;

        while (tower.getLevel() < towerDescriptor.getLevel()) {
            tower.enhance();
        }

        tower.setPlateau((Plateau) getGameEngine().getEntityById(towerDescriptor.getPlateauId()));
        tower.setValue(towerDescriptor.getValue());
        tower.setDamageInflicted(towerDescriptor.getDamageInflicted());
        tower.setEnabled(true);

        return tower;
    }

}
