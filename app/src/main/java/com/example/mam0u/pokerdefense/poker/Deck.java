package com.example.mam0u.pokerdefense.poker;
import com.example.mam0u.pokerdefense.R;

import java.util.ArrayList;
import java.util.Random;

public class Deck {
    private ArrayList<Card> deck = new ArrayList<Card>();
    public Deck (boolean jokerb){
        Card Ace1 = new Card("hearts",13, R.drawable.asheart);
        Card Ace2 = new Card("spades",13, R.drawable.asspades);
        Card Ace3 = new Card("clubs",13, R.drawable.asclub);
        Card Ace4 = new Card("diamonds",13, R.drawable.asdiamont);
        Card King1 = new Card("hearts",12, R.drawable.kheart);
        Card King2 = new Card("spades",12, R.drawable.kspades);
        Card King3 = new Card("clubs",12, R.drawable.kclub);
        Card King4 = new Card("diamonds",12, R.drawable.kdiamont);
        Card Queen1 = new Card("hearts",11, R.drawable.qheart);
        Card Queen2 = new Card("spades",11, R.drawable.qspades);
        Card Queen3 = new Card("clubs",11, R.drawable.qclub);
        Card Queen4 = new Card("diamonds",11, R.drawable.qdiamont);
        Card Valet1 = new Card("hearts",10, R.drawable.vheart);
        Card Valet2 = new Card("spades",10,R.drawable.vspades );
        Card Valet3 = new Card("clubs",10, R.drawable.vclub);
        Card Valet4 = new Card("diamonds",10, R.drawable.vdiamont);
        Card ten1= new Card("hearts",9,R.drawable.heart10);
        Card ten2 = new Card("spades",9,R.drawable.spades10);
        Card ten3 = new Card("clubs",9,R.drawable.club10);
        Card ten4 = new Card("diamonds",9,R.drawable.diamont10);
        Card nine1= new Card("hearts",8, R.drawable.heart9);
        Card nine2 = new Card("spades",8,R.drawable.spades9);
        Card nine3 = new Card("clubs",8, R.drawable.club9);
        Card nine4 = new Card("diamonds",8, R.drawable.diamont9);
        Card eight1= new Card("hearts",7,R.drawable.heart8);
        Card eight2 = new Card("spades",7,R.drawable.spades8);
        Card eight3 = new Card("clubs",7,R.drawable.club8);
        Card eight4 = new Card("diamonds",7,R.drawable.diamont8);
        Card seven1= new Card("hearts",6, R.drawable.heart7);
        Card seven2 = new Card("spades",6, R.drawable.spades7);
        Card seven3 = new Card("clubs",6, R.drawable.club7);
        Card seven4 = new Card("diamonds",6,R.drawable.diamont7);
        Card six1= new Card("hearts",5,R.drawable.heart6);
        Card six2 = new Card("spades",5,R.drawable.spades6);
        Card six3 = new Card("clubs",5, R.drawable.club6);
        Card six4 = new Card("diamonds",5,R.drawable.diamont6);
        Card five1= new Card("hearts",4,R.drawable.heart5);
        Card five2 = new Card("spades",4,R.drawable.spades5);
        Card five3 = new Card("clubs",4, R.drawable.club5);
        Card five4 = new Card("diamonds",4, R.drawable.diamont5);
        Card four1= new Card("hearts",3,R.drawable.heart4);
        Card four2 = new Card("spades",3,R.drawable.spades4);
        Card four3 = new Card("clubs",3,R.drawable.club4);
        Card four4 = new Card("diamonds",3,R.drawable.diamont4);
        Card three1= new Card("hearts",2,R.drawable.heart3);
        Card three2 = new Card("spades",2,R.drawable.spades3);
        Card three3 = new Card("clubs",2,R.drawable.club3);
        Card three4 = new Card("diamonds",2,R.drawable.diamont3);
        Card two1= new Card("hearts",1,R.drawable.heart2);
        Card two2 = new Card("spades",1,R.drawable.spades2);
        Card two3 = new Card("clubs",1,R.drawable.club2);
        Card two4 = new Card("diamonds",1,R.drawable.diamont2);
        Card joker1 =new Card("none",0, R.drawable.jokera);
        Card joker2 =new Card("none",0, R.drawable.jokerb);
        if (jokerb) {
            Card[] deckt = {Ace1,Ace2,Ace3,Ace4,King1,King2,King3,King4,Queen1,Queen2,Queen3,Queen4,Valet1,Valet2,Valet3,Valet4,ten1,ten2,ten3,ten4,nine1,nine2,nine3,nine4,eight1,eight2,eight3,eight4,seven1,seven2,seven3,seven4,six1,six2,six3,six4,five1,five2,five3,five4,four1,four2,four3,four4,three1,three2,three3,three4,two1,two2,two3,two4,joker1,joker2};
            for(int index=0; index < deckt.length;index++)
                deck.add(deckt[index]);
        }else{
            Card[] deckt = {Ace1,Ace2,Ace3,Ace4,King1,King2,King3,King4,Queen1,Queen2,Queen3,Queen4,Valet1,Valet2,Valet3,Valet4,ten1,ten2,ten3,ten4,nine1,nine2,nine3,nine4,eight1,eight2,eight3,eight4,seven1,seven2,seven3,seven4,six1,six2,six3,six4,five1,five2,five3,five4,four1,four2,four3,four4,three1,three2,three3,three4,two1,two2,two3,two4};
            for(int index=0; index < deckt.length;index++)
                deck.add(deckt[index]);
        }
    }

    public  void shuffle(){
        ArrayList<Card> shuffledeck = new ArrayList<Card>();
        while(!deck.isEmpty()){
            Random rand=new Random();
            int card=rand.nextInt(deck.size());
            shuffledeck.add(deck.get(card));
            deck.remove(card);
        }
        this.deck=shuffledeck;
    }
    public void setDeck(ArrayList<Card> deck) {
        this.deck = deck;
    }
    public ArrayList<Card> getDeck() {
        return deck;

    }
}
