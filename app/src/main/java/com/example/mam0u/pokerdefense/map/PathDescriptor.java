package com.example.mam0u.pokerdefense.map;

import com.example.mam0u.pokerdefense.math.Vector2;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Root
public class PathDescriptor {
    @ElementList(entry = "wayPoint", inline = true)
    private List<Vector2> wayPoints = new ArrayList<>();

    public List<Vector2> getWayPoints() {
        return Collections.unmodifiableList(wayPoints);
    }
}
