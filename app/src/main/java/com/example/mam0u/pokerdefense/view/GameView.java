package com.example.mam0u.pokerdefense.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;

import com.example.mam0u.pokerdefense.Application;
import com.example.mam0u.pokerdefense.builder.Factory;
import com.example.mam0u.pokerdefense.math.Vector2;
import com.example.mam0u.pokerdefense.tower.TowerInserter;
import com.example.mam0u.pokerdefense.tower.TowerSelector;
import com.example.mam0u.pokerdefense.tower.TowerView;


public class GameView extends View implements View.OnDragListener, View.OnTouchListener {
    private final Viewport mViewport;
    private final Renderer mRenderer;
    private final TowerSelector mTowerSelector;
    private final TowerInserter mTowerInserter;

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (!isInEditMode()) {
            Factory factory = Application.getInstance().getGameFactory();
            mViewport = factory.getViewport();
            mRenderer = factory.getRenderer();
            mTowerSelector = factory.getTowerSelector();
            mTowerInserter = factory.getTowerInserter();

            mRenderer.setView(this);
        } else {
            mViewport = null;
            mRenderer = null;
            mTowerSelector = null;
            mTowerInserter = null;
        }

        setFocusable(true);
        setOnDragListener(this);
        setOnTouchListener(this);
    }

    public void close() {
        mRenderer.setView(null);
    }

    @Override
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        if (!isInEditMode()) {
            mViewport.setScreenSize(w, h);
        }
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (!isInEditMode()) {
            mRenderer.draw(canvas);
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            Vector2 pos = mViewport.screenToGame(new Vector2(event.getX(), event.getY()));
            mTowerSelector.selectTowerAt(pos);
            return true;
        }

        return false;
    }

    @Override
    public boolean onDrag(View view, DragEvent event) {
        if (event.getAction() == DragEvent.ACTION_DRAG_STARTED) {
            if (event.getLocalState() instanceof TowerView) {
                if (event.getX() > 0 && event.getX() < getWidth() && event.getY() > 0 && event.getY() < getHeight()) {
                    Vector2 pos = mViewport.screenToGame(new Vector2(event.getX(), event.getY()));
                    mTowerInserter.setPosition(pos);
                }

                return true;
            }
        }

        if (event.getAction() == DragEvent.ACTION_DRAG_LOCATION) {
            Vector2 pos = mViewport.screenToGame(new Vector2(event.getX(), event.getY()));
            mTowerInserter.setPosition(pos);
        }

        if (event.getAction() == DragEvent.ACTION_DROP) {
            mTowerInserter.buyTower();
        }

        if (event.getAction() == DragEvent.ACTION_DRAG_EXITED || event.getAction() == DragEvent.ACTION_DRAG_ENDED) {
            mTowerInserter.cancel();
        }

        return false;
    }
}
