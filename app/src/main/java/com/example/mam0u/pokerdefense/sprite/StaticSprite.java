package com.example.mam0u.pokerdefense.sprite;

public class StaticSprite extends SpriteInstance {
    private int mIndex;

    StaticSprite(int layer, SpriteTemplate template) {
        super(layer, template);
    }

    public void setIndex(int index) {
        mIndex = index;
    }

    @Override
    int getIndex() {
        return mIndex;
    }
}
