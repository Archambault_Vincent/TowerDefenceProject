package com.example.mam0u.pokerdefense.game;


public interface GameStateListener {
    void gameRestart();
    void gameOver();
}
