package com.example.mam0u.pokerdefense.game;


public interface CreditsListener {
    void creditsChanged(int credits);
}
