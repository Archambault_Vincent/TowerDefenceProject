package com.example.mam0u.pokerdefense.wave;

import com.example.mam0u.pokerdefense.enemy.Enemy;
import com.example.mam0u.pokerdefense.entity.EntityRegistry;

import java.util.HashMap;
import java.util.Map;



public class EnemyDefaultHealth {

    private final EntityRegistry mEntityRegistry;
    private final Map<String, Float> mEnemyDefaultHealth;

    public EnemyDefaultHealth(EntityRegistry entityRegistry) {
        mEntityRegistry = entityRegistry;
        mEnemyDefaultHealth = new HashMap<>();
    }

    public float getDefaultHealth(String name) {
        if (!mEnemyDefaultHealth.containsKey(name)) {
            Enemy enemy = (Enemy) mEntityRegistry.createEntity(name);
            mEnemyDefaultHealth.put(name, enemy.getMaxHealth());
        }

        return mEnemyDefaultHealth.get(name);
    }

}
