package com.example.mam0u.pokerdefense.enemy;


import com.example.mam0u.pokerdefense.R;
import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.entity.Entity;
import com.example.mam0u.pokerdefense.entity.EntityFactory;
import com.example.mam0u.pokerdefense.entity.EntityRegistry;
import com.example.mam0u.pokerdefense.loop.TickListener;
import com.example.mam0u.pokerdefense.setting.EnemySettings;
import com.example.mam0u.pokerdefense.setting.EnemySettingsRoot;
import com.example.mam0u.pokerdefense.setting.GlobalSettings;
import com.example.mam0u.pokerdefense.sprite.AnimatedSprite;
import com.example.mam0u.pokerdefense.sprite.ReplicatedSprite;
import com.example.mam0u.pokerdefense.sprite.SpriteInstance;
import com.example.mam0u.pokerdefense.sprite.SpriteTemplate;
import com.example.mam0u.pokerdefense.sprite.SpriteTransformation;
import com.example.mam0u.pokerdefense.sprite.SpriteTransformer;
import com.example.mam0u.pokerdefense.view.Layers;

public class Flyer extends Enemy implements SpriteTransformation {

    private final static String ENTITY_NAME = "flyer";
    private final static float ANIMATION_SPEED = 1.0f;

    public static class Factory implements EntityFactory {
        @Override
        public String getEntityName() {
            return ENTITY_NAME;
        }

        @Override
        public Entity create(GameEngine gameEngine) {
            EnemySettingsRoot enemySettingsRoot = gameEngine.getGameConfiguration().getEnemySettingsRoot();
            return new Flyer(gameEngine, enemySettingsRoot.getGlobalSettings(), enemySettingsRoot.getFlyerSettings());
        }
    }

    public static class Persister extends EnemyPersister {
        public Persister(GameEngine gameEngine, EntityRegistry entityRegistry) {
            super(gameEngine, entityRegistry, ENTITY_NAME);
        }
    }

    private static class StaticData implements TickListener {
        SpriteTemplate mSpriteTemplate;
        AnimatedSprite mReferenceSprite;

        @Override
        public void tick() {
            mReferenceSprite.tick();
        }
    }

    private float mAngle;

    private ReplicatedSprite mSprite;

    private Flyer(GameEngine gameEngine, GlobalSettings globalSettings, EnemySettings enemySettings) {
        super(gameEngine, globalSettings, enemySettings);
        StaticData s = (StaticData) getStaticData();

        mSprite = getSpriteFactory().createReplication(s.mReferenceSprite);
        mSprite.setListener(this);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    @Override
    public Object initStatic() {
        StaticData s = new StaticData();

        s.mSpriteTemplate = getSpriteFactory().createTemplate(R.attr.flyer, 6);
        s.mSpriteTemplate.setMatrix(0.9f, 0.9f, null, -90f);

        s.mReferenceSprite = getSpriteFactory().createAnimated(Layers.ENEMY, s.mSpriteTemplate);
        s.mReferenceSprite.setSequenceForwardBackward();
        s.mReferenceSprite.setFrequency(ANIMATION_SPEED);

        getGameEngine().add(s);

        return s;
    }

    @Override
    public void init() {
        super.init();

        getGameEngine().add(mSprite);
    }

    @Override
    public void clean() {
        super.clean();

        getGameEngine().remove(mSprite);
    }

    @Override
    public void tick() {
        super.tick();

        if (hasWayPoint()) {
            mAngle = getDirection().angle();
        }
    }

    @Override
    public void draw(SpriteInstance sprite, SpriteTransformer transformer) {
        transformer.translate(getPosition());
        transformer.rotate(mAngle);
    }
}
