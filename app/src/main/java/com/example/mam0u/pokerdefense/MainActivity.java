package com.example.mam0u.pokerdefense;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final int PERMISSIONS_REQUEST = 0;

    private static final String TAG = "MainActivity";

    private Location mLocation;

    private Boolean joker = false;

    private LocationManager locationManager;

    private LocationListener locationListener;

    String locationProvider = LocationManager.GPS_PROVIDER;



    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {

            public void onLocationChanged(Location location) {

                // Called when a new location is found by the location provider.

                Log.d(TAG, "Location changed");

                Toast.makeText(getApplicationContext(), "Location changed, updating map...",

                        Toast.LENGTH_SHORT).show();

                mLocation = location;

            }



            @Override

            public void onStatusChanged(String provider, int status, Bundle extras) {}



            @Override

            public void onProviderEnabled(String provider) {}



            @Override

            public void onProviderDisabled(String provider) {}

        };

    }


    @Override

    protected void onResume() {

        super.onResume();

        // Ask for permission.

        if (ContextCompat.checkSelfPermission(this,

                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


            // Should we show an explanation?

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,

                    Manifest.permission.ACCESS_FINE_LOCATION)) {



                // Show an explanation to the user *asynchronously* -- don't block

                // this thread waiting for the user's response! After the user

                // sees the explanation, try again to request the permission.


            } else {


                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,

                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},

                        PERMISSIONS_REQUEST);

            }


        } else {


            // Permission already granted, Do the

            // location-related task you need to do.


            // Request location updates.

            locationManager.requestLocationUpdates(locationProvider, 0, 0, locationListener);

        }


    }


    public void checkedjoker (View view){

        joker = ((CheckBox)view).isChecked();

    }


    public void next(View view) {

        int mapchoose ;

        if (mLocation == null) {

            mLocation = new Location(locationProvider);

            mLocation.setLatitude(0);

            mLocation.setLongitude(0);

        }

        if(mLocation.getLongitude() > -60) {

            if(mLocation.getLatitude() > 15){

                mapchoose =1 ;

            }else{

                mapchoose = 2;

            }


        }else{



        }

        Intent intent = new Intent(this, Pokeractivity.class);

        intent.putExtra("joker",joker);

        // Start the activity

        startActivity(intent);
    }
}
