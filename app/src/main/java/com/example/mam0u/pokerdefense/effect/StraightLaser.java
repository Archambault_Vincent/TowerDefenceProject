package com.example.mam0u.pokerdefense.effect;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.example.mam0u.pokerdefense.enemy.Enemy;
import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.entity.Entity;
import com.example.mam0u.pokerdefense.entity.Types;
import com.example.mam0u.pokerdefense.iterator.StreamIterator;
import com.example.mam0u.pokerdefense.math.Vector2;
import com.example.mam0u.pokerdefense.view.Drawable;
import com.example.mam0u.pokerdefense.view.Layers;


public class StraightLaser extends Effect {

    private final static float LASER_WIDTH = 0.7f;

    private final static float EFFECT_DURATION = 0.5f;
    private final static int ALPHA_START = 180;
    private final static int ALPHA_STEP = (int) (ALPHA_START / (GameEngine.TARGET_FRAME_RATE * EFFECT_DURATION));

    private class LaserDrawable implements Drawable {
        private Paint mPaint;
        private int mAlpha = ALPHA_START;

        public LaserDrawable() {
            mPaint = new Paint();
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeWidth(0.1f);
            mPaint.setColor(Color.RED);
        }

        public void decreaseVisibility() {
            mAlpha -= ALPHA_STEP;

            if (mAlpha < 0) {
                mAlpha = 0;
            }

            mPaint.setAlpha(mAlpha);
        }

        @Override
        public int getLayer() {
            return Layers.SHOT;
        }

        @Override
        public void draw(Canvas canvas) {
            canvas.drawLine(getPosition().x(), getPosition().y(), mLaserTo.x(), mLaserTo.y(), mPaint);
        }
    }

    private float mDamage;
    private Vector2 mLaserTo;

    private LaserDrawable mDrawObject;

    public StraightLaser(Entity origin, Vector2 position, Vector2 laserTo, float damage) {
        super(origin, EFFECT_DURATION);
        setPosition(position);

        mLaserTo = laserTo;
        mDamage = damage;

        mDrawObject = new LaserDrawable();
    }

    @Override
    public void init() {
        super.init();

        getGameEngine().add(mDrawObject);
    }

    @Override
    public void clean() {
        super.clean();

        getGameEngine().remove(mDrawObject);
    }

    @Override
    public void tick() {
        super.tick();

        mDrawObject.decreaseVisibility();
    }

    @Override
    protected void effectBegin() {
        StreamIterator<Enemy> enemies = getGameEngine().getEntitiesByType(Types.ENEMY)
                .filter(onLine(getPosition(), mLaserTo, LASER_WIDTH))
                .cast(Enemy.class);

        while (enemies.hasNext()) {
            Enemy enemy = enemies.next();
            enemy.damage(mDamage, getOrigin());
        }
    }
}
