package com.example.mam0u.pokerdefense;

import com.example.mam0u.pokerdefense.builder.Factory;

public class Application extends android.app.Application {
    private static Application sInstance;
    private Factory Fact;

    @Override
    public void onCreate() {
        super.onCreate();

        sInstance = this;
        Fact = new Factory(getApplicationContext());
    }

    public static Application getInstance() {
        return sInstance;
    }

    public Factory getGameFactory() {
        return Fact;
    }
}
