package com.example.mam0u.pokerdefense.loop;

public interface TickListener {
    void tick();
}
