package com.example.mam0u.pokerdefense;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.example.mam0u.pokerdefense.poker.Hand;


public class Pokeractivity extends AppCompatActivity {
    Hand hand;

    //private SharedPreferences sharedPref = null;




    public void BeginPoker(){

        Bundle bundle  = getIntent().getExtras();

        boolean joker = bundle.getBoolean("joker");

        hand= new Hand(joker);

        ImageView[] image= {
                (ImageView)findViewById(R.id.img1),
                (ImageView)findViewById(R.id.img2),
                (ImageView)findViewById(R.id.img3),
                (ImageView)findViewById(R.id.img4),
                (ImageView)findViewById(R.id.img5),
        };

        int i;

        for(i=0 ; i < 5 ; i++){

            image[i].setImageResource(hand.getHand()[i].getImage());

        }

    }



    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pokeractivity);


        BeginPoker();

    }



    public void SwitchCard(View button){

        int idbutton=button.getId();

        int idimage=0;

        switch(idbutton){


            case R.id.change1:

                idbutton=0;

                idimage=R.id.img1;

                break;


            case R.id.change2:

                idbutton=1;

                idimage=R.id.img2;

                break;


            case R.id.change3:

                idbutton=2;

                idimage=R.id.img3;

                break;


            case R.id.change4:

                idbutton=3;

                idimage=R.id.img4;

                break;


            case R.id.change5:

                idbutton=4;

                idimage=R.id.img5;

                break;

            default : idbutton=0;

        }

        ImageView image = (ImageView) findViewById(idimage);

        if(!hand.getHand()[idbutton].isChange()){

            hand.ChangeCard(idbutton);

            image.setImageResource(hand.getHand()[idbutton].getImage());

            hand.getHand()[idbutton].setChange(true);

            button.setVisibility(View.INVISIBLE);

        }

    }



    protected void GO (View button){

        //SharedPreferences.Editor editor = sharedPref.edit();

        //editor.putInt("hand valor",hand.AnalizeHand());

        Intent intent = new Intent(this, GameActivity.class);

        startActivity(intent);

    }


}
