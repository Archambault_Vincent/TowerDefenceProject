package com.example.mam0u.pokerdefense.effect;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.example.mam0u.pokerdefense.enemy.Enemy;
import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.entity.Entity;
import com.example.mam0u.pokerdefense.entity.Types;
import com.example.mam0u.pokerdefense.iterator.StreamIterator;
import com.example.mam0u.pokerdefense.math.Vector2;
import com.example.mam0u.pokerdefense.view.Drawable;
import com.example.mam0u.pokerdefense.view.Layers;

import java.util.Collection;



public class HealEffect extends Effect {

    private static final float EFFECT_DURATION = 0.7f;

    private class HealDrawable implements Drawable {
        private Paint mPaint;

        public HealDrawable() {
            mPaint = new Paint();
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeWidth(0.05f);
            mPaint.setColor(Color.BLUE);
            mPaint.setAlpha(70);
        }

        @Override
        public int getLayer() {
            return Layers.SHOT;
        }

        @Override
        public void draw(Canvas canvas) {
            canvas.drawCircle(getPosition().x(), getPosition().y(), mDrawRadius, mPaint);
        }
    }

    private float mRange;
    private float mDrawRadius;
    private float mHealAmount;

    private Drawable mDrawable;
    private Collection<Enemy> mHealedEnemies;

    public HealEffect(Entity origin, Vector2 position, float amount, float radius, Collection<Enemy> healedEnemies) {
        super(origin, EFFECT_DURATION);
        setPosition(position);

        mHealAmount = amount;
        mRange = radius;
        mDrawRadius = 0f;
        mHealedEnemies = healedEnemies;

        mDrawable = new HealDrawable();
    }

    @Override
    public void init() {
        super.init();

        getGameEngine().add(mDrawable);
    }

    @Override
    public void clean() {
        super.clean();

        getGameEngine().remove(mDrawable);
    }

    @Override
    public void tick() {
        super.tick();

        mDrawRadius += mRange / (GameEngine.TARGET_FRAME_RATE * EFFECT_DURATION);
    }

    @Override
    protected void effectBegin() {
        StreamIterator<Enemy> enemies = getGameEngine().getEntitiesByType(Types.ENEMY)
                .filter(inRange(getPosition(), mRange))
                .filter(mHealedEnemies)
                .cast(Enemy.class);

        while (enemies.hasNext()) {
            Enemy enemy = enemies.next();
            enemy.heal(mHealAmount * enemy.getMaxHealth());
            mHealedEnemies.add(enemy);
        }
    }
}
