package com.example.mam0u.pokerdefense.tower;

import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.entity.Types;
import com.example.mam0u.pokerdefense.loop.Message;

import java.util.Iterator;



public class TowerAging {

    private final GameEngine mGameEngine;

    public TowerAging(GameEngine gameEngine) {
        mGameEngine = gameEngine;
    }

    public void ageTowers() {
        if (mGameEngine.isThreadChangeNeeded()) {
            mGameEngine.post(new Message() {
                @Override
                public void execute() {
                    ageTowers();
                }
            });
            return;
        }

        Iterator<Tower> towers = mGameEngine
                .getEntitiesByType(Types.TOWER)
                .cast(Tower.class);

        while (towers.hasNext()) {
            Tower tower = towers.next();
            ageTower(tower);
        }
    }

    public void ageTower(final Tower tower) {
        if (mGameEngine.isThreadChangeNeeded()) {
            mGameEngine.post(new Message() {
                @Override
                public void execute() {
                    ageTower(tower);
                }
            });
            return;
        }

        int value = tower.getValue();
        value = Math.round(value * mGameEngine.getGameConfiguration().getTowerSettingsRoot().getAgeModifier());
        tower.setValue(value);
    }
}
