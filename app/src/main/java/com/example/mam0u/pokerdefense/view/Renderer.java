package com.example.mam0u.pokerdefense.view;

import android.graphics.Canvas;
import android.view.View;

import com.example.mam0u.pokerdefense.R;
import com.example.mam0u.pokerdefense.container.SafeMultiMap;
import com.example.mam0u.pokerdefense.loop.FrameRateLogger;
import com.example.mam0u.pokerdefense.math.Vector2;
import com.example.mam0u.pokerdefense.theme.Theme;
import com.example.mam0u.pokerdefense.theme.ThemeListener;
import com.example.mam0u.pokerdefense.theme.ThemeManager;

import java.lang.ref.WeakReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class Renderer implements ThemeListener {
    private final Viewport mViewport;
    private final FrameRateLogger mFrameRateLogger;
    private final SafeMultiMap<Drawable> mDrawables = new SafeMultiMap<>();
    private final Lock mLock = new ReentrantLock(true);

    private int mBackgroundColor;
    private WeakReference<View> mViewRef;

    public Renderer(Viewport viewport, ThemeManager themeManager, FrameRateLogger frameRateLogger) {
        mViewport = viewport;
        mFrameRateLogger = frameRateLogger;
        themeManager.addListener(this);
        themeChanged(themeManager.getTheme());
    }

    public void setView(final View view) {
        mViewRef = new WeakReference<>(view);
    }

    public void add(Drawable obj) {
        mDrawables.add(obj.getLayer(), obj);
    }

    public void remove(Drawable obj) {
        mDrawables.remove(obj.getLayer(), obj);
    }

    public void clear() {
        mDrawables.clear();
    }

    public void lock() {
        mLock.lock();
    }

    public void unlock() {
        mLock.unlock();
    }

    public void invalidate() {
        View view = mViewRef.get();

        if (view != null) {
            view.postInvalidate();
        }
    }

    public void draw(Canvas canvas) {
        mLock.lock();

        canvas.drawColor(mBackgroundColor);
        canvas.concat(mViewport.getScreenMatrix());

        for (Drawable obj : mDrawables) {
            obj.draw(canvas);
        }

        mLock.unlock();

        mFrameRateLogger.incrementRenderCount();
    }

    @Override
    public void themeChanged(Theme theme) {
        mBackgroundColor = theme.getColor(R.attr.backgroundColor);
    }

    public boolean isPositionVisible(Vector2 position) {
        return mViewport.getScreenClipRect().contains(position.x(), position.y());
    }
}
