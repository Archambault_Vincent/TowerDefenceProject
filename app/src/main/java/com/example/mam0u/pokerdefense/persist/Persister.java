package com.example.mam0u.pokerdefense.persist;

import com.example.mam0u.pokerdefense.game.GameDescriptorRoot;


public interface Persister {
    void writeDescriptor(GameDescriptorRoot gameDescriptor);
    void readDescriptor(GameDescriptorRoot gameDescriptor);
}
