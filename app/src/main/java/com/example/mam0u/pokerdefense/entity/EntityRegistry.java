package com.example.mam0u.pokerdefense.entity;

import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.game.GameDescriptorRoot;
import com.example.mam0u.pokerdefense.persist.Persister;

import java.util.HashMap;
import java.util.Map;


public class EntityRegistry implements Persister{
    private final GameEngine mGameEngine;
    private final Map<String, EntityFactory> mEntityFactories = new HashMap<>();

    private int mNextEntityId = 1;

    public EntityRegistry(GameEngine gameEngine) {
        mGameEngine = gameEngine;
    }

    public void registerEntity(EntityFactory factory) {
        mEntityFactories.put(factory.getEntityName(), factory);
    }

    public Entity createEntity(String name) {
        return createEntity(name, mNextEntityId++);
    }

    public Entity createEntity(String name, int id) {
        Entity entity = mEntityFactories.get(name).create(mGameEngine);
        entity.setEntityId(id);
        return entity;
    }

    @Override
    public void writeDescriptor(GameDescriptorRoot gameDescriptor) {
        gameDescriptor.setNextEntityId(mNextEntityId);
    }

    @Override
    public void readDescriptor(GameDescriptorRoot gameDescriptor) {
        mNextEntityId = gameDescriptor.getNextEntityId();
    }
}
