package com.example.mam0u.pokerdefense.enemy;


import com.example.mam0u.pokerdefense.R;
import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.entity.Entity;
import com.example.mam0u.pokerdefense.entity.EntityFactory;
import com.example.mam0u.pokerdefense.entity.EntityRegistry;
import com.example.mam0u.pokerdefense.loop.TickListener;
import com.example.mam0u.pokerdefense.math.Function;
import com.example.mam0u.pokerdefense.math.SampledFunction;
import com.example.mam0u.pokerdefense.setting.EnemySettings;
import com.example.mam0u.pokerdefense.setting.EnemySettingsRoot;
import com.example.mam0u.pokerdefense.setting.GlobalSettings;
import com.example.mam0u.pokerdefense.sprite.AnimatedSprite;
import com.example.mam0u.pokerdefense.sprite.ReplicatedSprite;
import com.example.mam0u.pokerdefense.sprite.SpriteInstance;
import com.example.mam0u.pokerdefense.sprite.SpriteTemplate;
import com.example.mam0u.pokerdefense.sprite.SpriteTransformation;
import com.example.mam0u.pokerdefense.sprite.SpriteTransformer;
import com.example.mam0u.pokerdefense.view.Layers;

public class Sprinter extends Enemy implements SpriteTransformation {

    private final static String ENTITY_NAME = "sprinter";
    private final static float ANIMATION_SPEED = 0.7f;

    public static class Factory implements EntityFactory {
        @Override
        public String getEntityName() {
            return ENTITY_NAME;
        }

        @Override
        public Entity create(GameEngine gameEngine) {
            EnemySettingsRoot enemySettingsRoot = gameEngine.getGameConfiguration().getEnemySettingsRoot();
            return new Sprinter(gameEngine, enemySettingsRoot.getGlobalSettings(), enemySettingsRoot.getSprinterSettings());
        }
    }

    public static class Persister extends EnemyPersister {
        public Persister(GameEngine gameEngine, EntityRegistry entityRegistry) {
            super(gameEngine, entityRegistry, ENTITY_NAME);
        }
    }

    private static class StaticData implements TickListener {
        SampledFunction mSpeedFunction;

        SpriteTemplate mSpriteTemplate;
        AnimatedSprite mReferenceSprite;

        @Override
        public void tick() {
            mReferenceSprite.tick();
            mSpeedFunction.step();
        }
    }

    private float mAngle;
    private StaticData mStatic;
    private ReplicatedSprite mSprite;

    private Sprinter(GameEngine gameEngine, GlobalSettings globalSettings, EnemySettings enemySettings) {
        super(gameEngine, globalSettings, enemySettings);
        mStatic = (StaticData) getStaticData();

        mSprite = getSpriteFactory().createReplication(mStatic.mReferenceSprite);
        mSprite.setListener(this);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    @Override
    public Object initStatic() {
        StaticData s = new StaticData();

        s.mSpeedFunction = Function.sine()
                .multiply(0.9f)
                .offset(0.1f)
                .repeat((float) Math.PI)
                .stretch(GameEngine.TARGET_FRAME_RATE / ANIMATION_SPEED / (float) Math.PI)
                .sample();

        s.mSpriteTemplate = getSpriteFactory().createTemplate(R.attr.sprinter, 6);
        s.mSpriteTemplate.setMatrix(0.9f, 0.9f, null, null);

        s.mReferenceSprite = getSpriteFactory().createAnimated(Layers.ENEMY, s.mSpriteTemplate);
        s.mReferenceSprite.setSequenceForwardBackward();
        s.mReferenceSprite.setFrequency(ANIMATION_SPEED);

        getGameEngine().add(s);

        return s;
    }

    @Override
    public void init() {
        super.init();

        getGameEngine().add(mSprite);
    }

    @Override
    public void draw(SpriteInstance sprite, SpriteTransformer transformer) {
        transformer.translate(getPosition());
        transformer.rotate(mAngle);
    }

    @Override
    public void clean() {
        super.clean();

        getGameEngine().remove(mSprite);
    }

    @Override
    public void tick() {
        super.tick();

        if (hasWayPoint()) {
            mAngle = getDirection().angle();
        }
    }

    @Override
    public float getSpeed() {
        return super.getSpeed() * mStatic.mSpeedFunction.getValue();
    }
}
