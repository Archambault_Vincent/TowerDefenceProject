package com.example.mam0u.pokerdefense.game;


public interface BonusListener {
    void bonusChanged(int waveBonus, int earlyBonus);
}
