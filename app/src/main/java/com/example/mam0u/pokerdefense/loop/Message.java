package com.example.mam0u.pokerdefense.loop;

public interface Message {
    void execute();
}
