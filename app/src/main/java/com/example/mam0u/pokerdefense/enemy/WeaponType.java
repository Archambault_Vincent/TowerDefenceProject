package com.example.mam0u.pokerdefense.enemy;

public enum WeaponType {
    None,
    Bullet,
    Laser,
    Explosive
}
