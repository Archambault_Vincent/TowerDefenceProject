package com.example.mam0u.pokerdefense.enemy;

public interface EnemyListener {
    void enemyKilled(Enemy enemy);
    void enemyFinished(Enemy enemy);
    void enemyRemoved(Enemy enemy);
}
