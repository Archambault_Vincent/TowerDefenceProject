package com.example.mam0u.pokerdefense.poker;
import java.util.ArrayList;

public class Hand {
    private Card[] hand;
    private Deck deck;

    public Hand(boolean joker) {
        this.deck=new Deck(joker);
        this.deck.shuffle();
        Card[] hands={deck.getDeck().get(0),deck.getDeck().get(1),deck.getDeck().get(2),deck.getDeck().get(3),deck.getDeck().get(4)};
        this.hand=hands;
        deck.getDeck().remove(0);
        deck.getDeck().remove(0);
        deck.getDeck().remove(0);
        deck.getDeck().remove(0);
        deck.getDeck().remove(0);
    }
    public void setHand(Card[] hand) {
        this.hand = hand;
    }
    public Card[] getHand() {
        return hand;
    }
    /**
     * draw and discard a Card
     * @param index which card we discard
     */
    public void ChangeCard(int index){
        hand[index]=deck.getDeck().get(0);
        hand[index].setChange(true);
        deck.getDeck().remove(0);
    }
    public int AnalizeHand(){
        int handpower=0;
        int pairtype=0;
        int tripletype=0;
        ArrayList<Card> sorthand = new ArrayList<Card>();
        int index=0;
        int joker = 0;
        int jokerused=0;
        boolean jokerfh=false;
        boolean pair = false;
        boolean doublepair = false;
        boolean triple = false;
        boolean square = false;
        boolean straight=true;
        boolean flush=true;
        boolean fullhouse=false;
        boolean fivecard=false;
        //sort hand
        for(int index2=0;index2 < 5 ; index2++){
            if(hand[index2].getType()==0)
                joker++;
            index=0;
            while(index<sorthand.size() && hand[index2].getType() > sorthand.get(index).getType()){
                index++;
            }
            sorthand.add(index, hand[index2]);
        }
        int actualcard=sorthand.get(joker).getType();
        int numbercard=joker+1;
        String actualcolor=sorthand.get(joker).getColor();
        int bestcard = sorthand.get(joker).getType();
        for(int indexsort = joker+1 ; indexsort < 5 ;indexsort++){
            //check flush break
            if(!sorthand.get(indexsort).getColor().equals(actualcolor))
                flush=false;
            if(sorthand.get(indexsort).getType() != actualcard){
                if(bestcard < sorthand.get(indexsort).getType())
                    bestcard = sorthand.get(indexsort).getType();
                numbercard=joker+1;
                //check straight break
                if(sorthand.get(indexsort).getType()-actualcard > 1+joker-jokerused){
                    jokerused+=sorthand.get(indexsort).getType()-actualcard;
                    if(jokerused > joker){
                        straight=false;
                        jokerused=joker;
                    }
                }
                actualcard=sorthand.get(indexsort).getType();
            }
            else{
                numbercard++;
            }
            // analyse number card combinaison
            switch (numbercard) {
                case 2 :
                    //check double pair possibility
                    if(pair==true  && joker==0 && !triple){
                        if(bestcard < sorthand.get(indexsort).getType())
                            bestcard = sorthand.get(indexsort).getType();
                        doublepair=true;
                    }
                    else
                        bestcard = sorthand.get(indexsort).getType();
                    //check for full house
                    if(pairtype == 0)
                        pairtype=sorthand.get(indexsort).getType();
                    else{
                        if(triple && tripletype != pairtype && !jokerfh)
                            fullhouse=true;
                    }
                    pair=true;
                    break;
                case 3 :
                    if(!triple)
                        bestcard = sorthand.get(indexsort).getType();
                    if(numbercard-joker < 3)
                        jokerfh=true;
                    triple=true;
                    tripletype=actualcard;
                    if(pairtype != tripletype && !jokerfh)
                        fullhouse=true;
                    else
                        pairtype=0;
                    break;
                case 4 :
                    square=true;
                    break;
                case 5 :
                    fivecard=true;
                    break;
                default:
                    break;
            }
        }
        // analyse result
        if(pair)
            handpower=1;
        if(doublepair)
            handpower=2;
        if(triple)
            handpower=3;
        if(straight)
            handpower=4;
        if(flush)
            handpower=5;
        if(fullhouse)
            handpower=6;
        if(square)
            handpower=7;
        if(straight && flush)
            if(bestcard==13)
                handpower=9;
            else
                handpower=8;
        if(fivecard)
            handpower=14;
        return handpower;
    }
    public Deck getDeck() {
        return deck;
    }
}
