package com.example.mam0u.pokerdefense.wave;


public interface WaveListener {
    void waveNumberChanged();
    void nextWaveReadyChanged();
    void remainingEnemiesCountChanged();
}
