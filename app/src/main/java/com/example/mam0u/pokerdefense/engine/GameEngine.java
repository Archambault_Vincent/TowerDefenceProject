package com.example.mam0u.pokerdefense.engine;

import com.example.mam0u.pokerdefense.entity.Entity;
import com.example.mam0u.pokerdefense.entity.EntityStore;
import com.example.mam0u.pokerdefense.iterator.StreamIterator;
import com.example.mam0u.pokerdefense.loop.GameLoop;
import com.example.mam0u.pokerdefense.loop.Message;
import com.example.mam0u.pokerdefense.loop.MessageQueue;
import com.example.mam0u.pokerdefense.loop.TickListener;
import com.example.mam0u.pokerdefense.math.Vector2;
import com.example.mam0u.pokerdefense.sound.SoundFactory;
import com.example.mam0u.pokerdefense.sprite.SpriteFactory;
import com.example.mam0u.pokerdefense.theme.ThemeManager;
import com.example.mam0u.pokerdefense.view.Drawable;
import com.example.mam0u.pokerdefense.view.Renderer;


public class GameEngine {
    public final static int TARGET_FRAME_RATE = GameLoop.TARGET_FRAME_RATE;

    private final SpriteFactory mSpriteFactory;
    private final ThemeManager mThemeManager;
    private final SoundFactory mSoundFactory;

    private final EntityStore mEntityStore;
    private final MessageQueue mMessageQueue;
    private final Renderer mRenderer;
    private final GameLoop mGameLoop;

    private GameConfiguration mGameConfiguration;

    public GameEngine(SpriteFactory spriteFactory, ThemeManager themeManager,
                      SoundFactory soundFactory, EntityStore entityStore, MessageQueue messageQueue,
                      Renderer renderer, GameLoop gameLoop) {
        mSpriteFactory = spriteFactory;
        mThemeManager = themeManager;
        mSoundFactory = soundFactory;
        mEntityStore = entityStore;
        mMessageQueue = messageQueue;
        mRenderer = renderer;
        mGameLoop = gameLoop;

        mGameLoop.add(mMessageQueue);
        mGameLoop.add(mEntityStore);
    }

    public SpriteFactory getSpriteFactory() {
        return mSpriteFactory;
    }

    public ThemeManager getThemeManager() {
        return mThemeManager;
    }

    public SoundFactory getSoundFactory() {
        return mSoundFactory;
    }

    public Object getStaticData(Entity entity) {
        return mEntityStore.getStaticData(entity);
    }

    public StreamIterator<Entity> getAllEntities() {
        return mEntityStore.getAll();
    }

    public StreamIterator<Entity> getEntitiesByType(int typeId) {
        return mEntityStore.getByType(typeId);
    }

    public Entity getEntityById(int entityId) {
        return mEntityStore.getById(entityId);
    }

    public void add(Entity entity) {
        mEntityStore.add(entity);
    }

    public void add(Drawable drawable) {
        mRenderer.add(drawable);
    }

    public void add(TickListener listener) {
        mGameLoop.add(listener);
    }

    public void remove(Entity entity) {
        mEntityStore.remove(entity);
    }

    public void remove(Drawable drawable) {
        mRenderer.remove(drawable);
    }

    public void remove(TickListener listener) {
        mGameLoop.remove(listener);
    }

    public void clear() {
        mMessageQueue.clear();
        mEntityStore.clear();
        mRenderer.clear();
        mGameLoop.clear();

        mGameLoop.add(mMessageQueue);
        mGameLoop.add(mEntityStore);
    }

    public void start() {
        mGameLoop.start();
    }

    public void stop() {
        mGameLoop.stop();
    }

    public int getTickCount() {
        return mMessageQueue.getTickCount();
    }

    public void post(Message message) {
        mMessageQueue.post(message);
    }

    public void postDelayed(Message message, float delay) {
        mMessageQueue.postAfterTicks(message, Math.round(delay * TARGET_FRAME_RATE));
    }

    public void postAfterTicks(Message message, int ticks) {
        mMessageQueue.postAfterTicks(message, ticks);
    }

    public void setTicksPerLoop(int ticksPerLoop) {
        mGameLoop.setTicksPerLoop(ticksPerLoop);
    }

    public boolean isThreadChangeNeeded() {
        return mGameLoop.isThreadChangeNeeded();
    }

    public boolean isPositionVisible(Vector2 position) {
        return mRenderer.isPositionVisible(position);
    }

    public GameConfiguration getGameConfiguration() {
        return mGameConfiguration;
    }

    public void setGameConfiguration(GameConfiguration gameConfiguration) {
        mGameConfiguration = gameConfiguration;
    }
}
