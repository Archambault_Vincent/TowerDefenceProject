package com.example.mam0u.pokerdefense.game;

public interface LivesListener {
    void livesChanged(int lives);
}
