package com.example.mam0u.pokerdefense.tower;

public interface TowerInfoView {
    void showTowerInfo(TowerInfo towerInfo);
    void hideTowerInfo();
}
