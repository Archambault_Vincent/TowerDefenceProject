package com.example.mam0u.pokerdefense.projectile;


import com.example.mam0u.pokerdefense.R;
import com.example.mam0u.pokerdefense.effect.Explosion;
import com.example.mam0u.pokerdefense.enemy.Enemy;
import com.example.mam0u.pokerdefense.entity.Entity;
import com.example.mam0u.pokerdefense.entity.Types;
import com.example.mam0u.pokerdefense.math.RandomUtils;
import com.example.mam0u.pokerdefense.math.Vector2;
import com.example.mam0u.pokerdefense.sprite.AnimatedSprite;
import com.example.mam0u.pokerdefense.sprite.SpriteInstance;
import com.example.mam0u.pokerdefense.sprite.SpriteTemplate;
import com.example.mam0u.pokerdefense.sprite.SpriteTransformation;
import com.example.mam0u.pokerdefense.sprite.SpriteTransformer;
import com.example.mam0u.pokerdefense.sprite.StaticSprite;
import com.example.mam0u.pokerdefense.view.Layers;

public class Rocket extends HomingShot implements SpriteTransformation {

    private final static float MOVEMENT_SPEED = 2.5f;
    private final static float ANIMATION_SPEED = 3f;

    private class StaticData {
        SpriteTemplate mSpriteTemplate;
        SpriteTemplate mSpriteTemplateFire;
    }

    private float mDamage;
    private float mRadius;
    private float mAngle;

    private StaticSprite mSprite;
    private AnimatedSprite mSpriteFire;

    public Rocket(Entity origin, Vector2 position, float damage, float radius) {
        super(origin);
        setPosition(position);
        setSpeed(MOVEMENT_SPEED);
        setEnabled(false);

        mDamage = damage;
        mRadius = radius;

        StaticData s = (StaticData) getStaticData();

        mSprite = getSpriteFactory().createStatic(Layers.SHOT, s.mSpriteTemplate);
        mSprite.setListener(this);
        mSprite.setIndex(RandomUtils.next(4));

        mSpriteFire = getSpriteFactory().createAnimated(Layers.SHOT, s.mSpriteTemplateFire);
        mSpriteFire.setListener(this);
        mSpriteFire.setSequenceForward();
        mSpriteFire.setFrequency(ANIMATION_SPEED);
    }

    public void setAngle(float angle) {
        mAngle = angle;
    }

    @Override
    public Object initStatic() {
        StaticData s = new StaticData();

        s.mSpriteTemplate = getSpriteFactory().createTemplate(R.attr.rocket, 4);
        s.mSpriteTemplate.setMatrix(0.8f, 1f, null, -90f);

        s.mSpriteTemplateFire = getSpriteFactory().createTemplate(R.attr.rocketFire, 4);
        s.mSpriteTemplateFire.setMatrix(0.3f, 0.3f, new Vector2(0.15f, 0.6f), -90f);

        return s;
    }

    @Override
    public void init() {
        super.init();

        getGameEngine().add(mSprite);

        if (isEnabled()) {
            getGameEngine().add(mSpriteFire);
        }
    }

    @Override
    public void clean() {
        super.clean();

        getGameEngine().remove(mSprite);

        if (isEnabled()) {
            getGameEngine().remove(mSpriteFire);
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        if (!isEnabled() && mSpriteFire != null) {
            getGameEngine().remove(mSpriteFire);
        }

        if (isEnabled() && mSpriteFire != null) {
            getGameEngine().add(mSpriteFire);
        }
    }

    public void draw(SpriteInstance sprite, SpriteTransformer transformer) {
        transformer.translate(getPosition());
        transformer.rotate(mAngle);
    }

    @Override
    public void tick() {
        if (isEnabled()) {
            setDirection(getDirectionTo(getTarget()));
            mAngle = getAngleTo(getTarget());

            mSpriteFire.tick();
        }

        super.tick();
    }

    @Override
    protected void targetLost() {
        Enemy closest = (Enemy) getGameEngine().getEntitiesByType(Types.ENEMY)
                .min(distanceTo(getPosition()));

        if (closest == null) {
            getGameEngine().remove(this);
        } else {
            setTarget(closest);
        }
    }

    @Override
    protected void targetReached() {
        getGameEngine().add(new Explosion(getOrigin(), getTarget().getPosition(), mDamage, mRadius));
        getGameEngine().remove(this);
    }
}
