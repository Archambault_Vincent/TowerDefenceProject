package com.example.mam0u.pokerdefense.tower;

public enum TowerStrategy {
    Closest,
    Weakest,
    Strongest,
    First,
    Last
}
