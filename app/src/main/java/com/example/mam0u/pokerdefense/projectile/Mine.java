package com.example.mam0u.pokerdefense.projectile;


import com.example.mam0u.pokerdefense.R;
import com.example.mam0u.pokerdefense.effect.Explosion;
import com.example.mam0u.pokerdefense.enemy.Enemy;
import com.example.mam0u.pokerdefense.enemy.Flyer;
import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.entity.Entity;
import com.example.mam0u.pokerdefense.entity.Types;
import com.example.mam0u.pokerdefense.iterator.Predicate;
import com.example.mam0u.pokerdefense.iterator.StreamIterator;
import com.example.mam0u.pokerdefense.loop.TickTimer;
import com.example.mam0u.pokerdefense.math.Function;
import com.example.mam0u.pokerdefense.math.RandomUtils;
import com.example.mam0u.pokerdefense.math.SampledFunction;
import com.example.mam0u.pokerdefense.math.Vector2;
import com.example.mam0u.pokerdefense.sprite.SpriteInstance;
import com.example.mam0u.pokerdefense.sprite.SpriteTemplate;
import com.example.mam0u.pokerdefense.sprite.SpriteTransformation;
import com.example.mam0u.pokerdefense.sprite.SpriteTransformer;
import com.example.mam0u.pokerdefense.sprite.StaticSprite;
import com.example.mam0u.pokerdefense.view.Layers;

public class Mine extends Shot implements SpriteTransformation {

    private final static float TRIGGER_RADIUS = 0.7f;

    private final static float TIME_TO_TARGET = 1.5f;
    private final static float ROTATION_RATE_MIN = 0.5f;
    private final static float ROTATION_RATE_MAX = 2.0f;
    private final static float HEIGHT_SCALING_START = 0.5f;
    private final static float HEIGHT_SCALING_STOP = 1.0f;
    private final static float HEIGHT_SCALING_PEAK = 1.5f;

    private class StaticData {
        SpriteTemplate mSpriteTemplate;
    }

    private float mDamage;
    private float mRadius;
    private float mAngle;
    private boolean mFlying;
    private float mRotationStep;
    private SampledFunction mHeightScalingFunction;
    private Vector2 mTarget;

    private StaticSprite mSpriteFlying;
    private StaticSprite mSpriteMine;

    private final TickTimer mUpdateTimer = TickTimer.createInterval(0.1f);

    public Mine(Entity origin, Vector2 position, Vector2 target, float damage, float radius) {
        super(origin);

        setPosition(position);
        setSpeed(getDistanceTo(target) / TIME_TO_TARGET);
        setDirection(getDirectionTo(target));

        mFlying = true;
        mDamage = damage;
        mRadius = radius;
        mTarget = target;

        mRotationStep = RandomUtils.next(ROTATION_RATE_MIN, ROTATION_RATE_MAX) * 360f / GameEngine.TARGET_FRAME_RATE;

        float x1 = (float) Math.sqrt(HEIGHT_SCALING_PEAK - HEIGHT_SCALING_START);
        float x2 = (float) Math.sqrt(HEIGHT_SCALING_PEAK - HEIGHT_SCALING_STOP);
        mHeightScalingFunction = Function.quadratic()
                .multiply(-1f)
                .offset(HEIGHT_SCALING_PEAK)
                .shift(-x1)
                .stretch(GameEngine.TARGET_FRAME_RATE * TIME_TO_TARGET / (x1 + x2))
                .sample();

        createAssets();
    }

    public Mine(Entity origin, Vector2 position, float damage, float radius) {
        super(origin);

        setPosition(position);

        mFlying = false;
        mDamage = damage;
        mRadius = radius;

        mHeightScalingFunction = Function.constant(HEIGHT_SCALING_STOP).sample();

        createAssets();
    }

    private void createAssets() {
        StaticData s = (StaticData) getStaticData();

        int index = RandomUtils.next(4);

        mSpriteFlying = getSpriteFactory().createStatic(Layers.SHOT, s.mSpriteTemplate);
        mSpriteFlying.setListener(this);
        mSpriteFlying.setIndex(index);

        mSpriteMine = getSpriteFactory().createStatic(Layers.BOTTOM, s.mSpriteTemplate);
        mSpriteMine.setListener(this);
        mSpriteMine.setIndex(index);
    }

    public Vector2 getTarget() {
        return mTarget;
    }

    @Override
    public Object initStatic() {
        StaticData s = new StaticData();

        s.mSpriteTemplate = getSpriteFactory().createTemplate(R.attr.mine, 4);
        s.mSpriteTemplate.setMatrix(0.7f, 0.7f, null, null);

        return s;
    }

    @Override
    public void init() {
        super.init();

        if (mFlying) {
            getGameEngine().add(mSpriteFlying);
        } else {
            getGameEngine().add(mSpriteMine);
        }
    }

    @Override
    public void clean() {
        super.clean();

        if (mFlying) {
            getGameEngine().remove(mSpriteFlying);
        } else {
            getGameEngine().remove(mSpriteMine);
        }
    }

    @Override
    public void tick() {
        super.tick();

        if (mFlying) {
            mAngle += mRotationStep;
            mHeightScalingFunction.step();

            if (mHeightScalingFunction.getPosition() >= GameEngine.TARGET_FRAME_RATE * TIME_TO_TARGET) {
                getGameEngine().remove(mSpriteFlying);
                getGameEngine().add(mSpriteMine);

                mFlying = false;
                setSpeed(0f);
            }
        } else if (mUpdateTimer.tick()) {
            StreamIterator<Enemy> enemiesInRange = getGameEngine().getEntitiesByType(Types.ENEMY)
                    .filter(inRange(getPosition(), TRIGGER_RADIUS))
                    .cast(Enemy.class)
                    .filter(new Predicate<Enemy>() {
                        @Override
                        public boolean apply(Enemy value) {
                            return !(value instanceof Flyer);
                        }
                    });

            if (!enemiesInRange.isEmpty()) {
                getGameEngine().add(new Explosion(getOrigin(), getPosition(), mDamage, mRadius));
                this.remove();
            }
        }
    }

    @Override
    public void draw(SpriteInstance sprite, SpriteTransformer transformer) {
        float s = mHeightScalingFunction.getValue();
        transformer.translate(getPosition());
        transformer.scale(s);
        transformer.rotate(mAngle);
    }
}
