package com.example.mam0u.pokerdefense.tower;

import com.example.mam0u.pokerdefense.entity.EntityRegistry;

import java.util.HashMap;
import java.util.Map;



public class TowerDefaultValue {

    private final EntityRegistry mEntityRegistry;
    private final Map<String, Integer> mTowerDefaultValue;

    public TowerDefaultValue(EntityRegistry entityRegistry) {
        mEntityRegistry = entityRegistry;
        mTowerDefaultValue = new HashMap<>();
    }

    public int getDefaultValue(String name) {
        if (!mTowerDefaultValue.containsKey(name)) {
            Tower tower = (Tower) mEntityRegistry.createEntity(name);
            mTowerDefaultValue.put(name, tower.getValue());
        }

        return mTowerDefaultValue.get(name);
    }

}
