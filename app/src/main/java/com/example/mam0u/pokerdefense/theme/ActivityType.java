package com.example.mam0u.pokerdefense.theme;

public enum ActivityType {
    Game,
    Popup,
    Normal
}
