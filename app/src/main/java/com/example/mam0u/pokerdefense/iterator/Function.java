package com.example.mam0u.pokerdefense.iterator;

public interface Function<F, T> {
    T apply(F input);
}
