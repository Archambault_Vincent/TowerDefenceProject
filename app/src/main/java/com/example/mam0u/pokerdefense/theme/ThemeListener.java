package com.example.mam0u.pokerdefense.theme;

public interface ThemeListener {
    void themeChanged(Theme theme);
}
