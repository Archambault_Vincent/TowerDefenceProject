package com.example.mam0u.pokerdefense.view;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mam0u.pokerdefense.Application;
import com.example.mam0u.pokerdefense.R;
import com.example.mam0u.pokerdefense.VPart;
import com.example.mam0u.pokerdefense.builder.Factory;
import com.example.mam0u.pokerdefense.game.GameState;
import com.example.mam0u.pokerdefense.game.GameStateListener;
import com.example.mam0u.pokerdefense.game.ScoreBoard;

import java.text.DecimalFormat;


public class GameOver extends VPart implements GameStateListener {
    private final GameState mGameState;
    private final ScoreBoard mScoreBoard;

    private Handler mHandler;

    private TextView txt_score;

    public GameOver() {
        Factory factory = Application.getInstance().getGameFactory();
        mGameState = factory.getGameState();
        mScoreBoard = factory.getScoreBoard();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_game_over, container, false);

        txt_score = (TextView) v.findViewById(R.id.txt_score);

        mHandler = new Handler();


        updateScore();

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mGameState.addListener(this);

        if (!mGameState.isGameOver()) {
            getFragmentManager().beginTransaction()
                    .hide(this)
                    .commit();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mGameState.removeListener(this);
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void gameRestart() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                getFragmentManager().beginTransaction()
                        .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                        .hide(GameOver.this)
                        .commitAllowingStateLoss();
            }
        });
    }

    @Override
    public void gameOver() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                updateScore();

                getFragmentManager().beginTransaction()
                        .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                        .show(GameOver.this)
                        .commitAllowingStateLoss();
            }
        });
    }

    private void updateScore() {
        DecimalFormat fmt = new DecimalFormat("###,###,###,###");
        txt_score.setText(getResources().getString(R.string.score) +
                ": " + fmt.format(mScoreBoard.getScore()));
    }
}
