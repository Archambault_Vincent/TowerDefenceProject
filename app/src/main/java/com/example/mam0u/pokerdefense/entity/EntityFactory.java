package com.example.mam0u.pokerdefense.entity;

import com.example.mam0u.pokerdefense.engine.GameEngine;


public interface EntityFactory {
    String getEntityName();
    Entity create(GameEngine gameEngine);
}
