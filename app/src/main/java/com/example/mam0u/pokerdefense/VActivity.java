package com.example.mam0u.pokerdefense;

import android.app.Activity;
import android.os.Bundle;

import com.example.mam0u.pokerdefense.builder.Factory;
import com.example.mam0u.pokerdefense.theme.ActivityType;
import com.example.mam0u.pokerdefense.theme.Theme;
import com.example.mam0u.pokerdefense.theme.ThemeListener;
import com.example.mam0u.pokerdefense.theme.ThemeManager;



public abstract class VActivity extends Activity implements ThemeListener {
    private final ThemeManager mThemeManager;

    public VActivity() {
        mThemeManager = getGameFactory().getThemeManager();
    }

    protected abstract ActivityType getActivityType();

    protected Factory getGameFactory() {
        return Application.getInstance().getGameFactory();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(mThemeManager.getTheme().getActivityThemeId(getActivityType()));
        super.onCreate(savedInstanceState);
        mThemeManager.addListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mThemeManager.removeListener(this);
    }

    @Override
    public void themeChanged(Theme theme) {
        recreate();
    }
}
