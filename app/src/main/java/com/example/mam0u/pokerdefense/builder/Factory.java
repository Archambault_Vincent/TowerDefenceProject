package com.example.mam0u.pokerdefense.builder;

import android.content.Context;

import com.example.mam0u.pokerdefense.enemy.Blob;
import com.example.mam0u.pokerdefense.enemy.Flyer;
import com.example.mam0u.pokerdefense.enemy.Healer;
import com.example.mam0u.pokerdefense.enemy.Soldier;
import com.example.mam0u.pokerdefense.enemy.Sprinter;
import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.entity.EntityRegistry;
import com.example.mam0u.pokerdefense.entity.EntityStore;
import com.example.mam0u.pokerdefense.game.GameConfigurationLoader;
import com.example.mam0u.pokerdefense.game.GameSpeed;
import com.example.mam0u.pokerdefense.game.GameState;
import com.example.mam0u.pokerdefense.game.Highscores;
import com.example.mam0u.pokerdefense.game.ScoreBoard;
import com.example.mam0u.pokerdefense.loop.FrameRateLogger;
import com.example.mam0u.pokerdefense.loop.GameLoop;
import com.example.mam0u.pokerdefense.loop.MessageQueue;
import com.example.mam0u.pokerdefense.map.MapRepository;
import com.example.mam0u.pokerdefense.persist.GamePersister;
import com.example.mam0u.pokerdefense.plateau.BasicPlateau;
import com.example.mam0u.pokerdefense.setting.SettingsManager;
import com.example.mam0u.pokerdefense.sound.SoundFactory;
import com.example.mam0u.pokerdefense.sound.SoundManager;
import com.example.mam0u.pokerdefense.sprite.SpriteFactory;
import com.example.mam0u.pokerdefense.theme.ThemeManager;
import com.example.mam0u.pokerdefense.tower.BouncingLaser;
import com.example.mam0u.pokerdefense.tower.Canon;
import com.example.mam0u.pokerdefense.tower.DualCanon;
import com.example.mam0u.pokerdefense.tower.GlueGun;
import com.example.mam0u.pokerdefense.tower.GlueTower;
import com.example.mam0u.pokerdefense.tower.MachineGun;
import com.example.mam0u.pokerdefense.tower.MineLayer;
import com.example.mam0u.pokerdefense.tower.Mortar;
import com.example.mam0u.pokerdefense.tower.RocketLauncher;
import com.example.mam0u.pokerdefense.tower.SimpleLaser;
import com.example.mam0u.pokerdefense.tower.StraightLaser;
import com.example.mam0u.pokerdefense.tower.Teleporter;
import com.example.mam0u.pokerdefense.tower.TowerAging;
import com.example.mam0u.pokerdefense.tower.TowerControl;
import com.example.mam0u.pokerdefense.tower.TowerInserter;
import com.example.mam0u.pokerdefense.tower.TowerSelector;
import com.example.mam0u.pokerdefense.view.Renderer;
import com.example.mam0u.pokerdefense.view.Viewport;
import com.example.mam0u.pokerdefense.wave.WaveManager;



public class Factory {

    private final ThemeManager mThemeManager;
    private final SoundManager mSoundManager;
    private final SpriteFactory mSpriteFactory;
    private final SoundFactory mSoundFactory;
    private final Viewport mViewport;
    private final FrameRateLogger mFrameRateLogger;
    private final EntityStore mEntityStore;
    private final MessageQueue mMessageQueue;
    private final Renderer mRenderer;
    private final GameEngine mGameEngine;
    private final GameLoop mGameLoop;
    private final GamePersister mGamePersister;
    private final EntityRegistry mEntityRegistry;

    private final ScoreBoard mScoreBoard;
    private final Highscores mHighScores;
    private final TowerSelector mTowerSelector;
    private final TowerControl mTowerControl;
    private final TowerAging mTowerAging;
    private final TowerInserter mTowerInserter;
    private final MapRepository mMapRepository;
    private final GameConfigurationLoader mGameConfigurationLoader;
    private final WaveManager mWaveManager;
    private final GameSpeed mSpeedManager;
    private final GameState mGameState;
    private final SettingsManager mSettingsManager;

    public Factory(Context context) {

        mThemeManager = new ThemeManager(context);
        mSoundManager = new SoundManager(context);
        mSpriteFactory = new SpriteFactory(context, mThemeManager);
        mSoundFactory = new SoundFactory(context, mSoundManager);
        mViewport = new Viewport();
        mFrameRateLogger = new FrameRateLogger();
        mEntityStore = new EntityStore();
        mMessageQueue = new MessageQueue();
        mRenderer = new Renderer(mViewport, mThemeManager, mFrameRateLogger);
        mGameLoop = new GameLoop(mRenderer, mFrameRateLogger);
        mGameEngine = new GameEngine(mSpriteFactory, mThemeManager, mSoundFactory, mEntityStore, mMessageQueue, mRenderer, mGameLoop);
        mEntityRegistry = new EntityRegistry(mGameEngine);
        mGamePersister = new GamePersister();

        registerEntities();


        mMapRepository = new MapRepository();
        mScoreBoard = new ScoreBoard(mGameEngine);
        mGameState = new GameState(mGameEngine, mThemeManager, mScoreBoard);
        mGameConfigurationLoader = new GameConfigurationLoader(context, mGameEngine, mScoreBoard, mGameState, mViewport, mEntityRegistry, mMapRepository);
        mTowerAging = new TowerAging(mGameEngine);
        mSpeedManager = new GameSpeed(mGameEngine);
        mWaveManager = new WaveManager(mGameEngine, mScoreBoard, mGameState, mEntityRegistry, mTowerAging);
        mHighScores = new Highscores(context, mGameState, mScoreBoard, mGameConfigurationLoader);
        mTowerSelector = new TowerSelector(mGameEngine, mGameState, mScoreBoard);
        mTowerControl = new TowerControl(mGameEngine, mScoreBoard, mTowerSelector, mEntityRegistry);
        mTowerInserter = new TowerInserter(mGameEngine, mGameState, mEntityRegistry, mTowerSelector, mTowerAging, mScoreBoard);
        mSettingsManager = new SettingsManager(context, mThemeManager, mSoundManager);

        registerPersisters();

        mGameState.restart();
    }

    private void registerEntities() {
        mEntityRegistry.registerEntity(new BasicPlateau.Factory());

        mEntityRegistry.registerEntity(new Blob.Factory());
        mEntityRegistry.registerEntity(new Flyer.Factory());
        mEntityRegistry.registerEntity(new Healer.Factory());
        mEntityRegistry.registerEntity(new Soldier.Factory());
        mEntityRegistry.registerEntity(new Sprinter.Factory());

        mEntityRegistry.registerEntity(new Canon.Factory());
        mEntityRegistry.registerEntity(new DualCanon.Factory());
        mEntityRegistry.registerEntity(new MachineGun.Factory());
        mEntityRegistry.registerEntity(new SimpleLaser.Factory());
        mEntityRegistry.registerEntity(new BouncingLaser.Factory());
        mEntityRegistry.registerEntity(new StraightLaser.Factory());
        mEntityRegistry.registerEntity(new Mortar.Factory());
        mEntityRegistry.registerEntity(new MineLayer.Factory());
        mEntityRegistry.registerEntity(new RocketLauncher.Factory());
        mEntityRegistry.registerEntity(new GlueTower.Factory());
        mEntityRegistry.registerEntity(new GlueGun.Factory());
        mEntityRegistry.registerEntity(new Teleporter.Factory());

    }

    private void registerPersisters() {
        mGamePersister.registerPersister(mEntityRegistry);
        mGamePersister.registerPersister(mMessageQueue);
        mGamePersister.registerPersister(mGameConfigurationLoader);
        mGamePersister.registerPersister(mScoreBoard);
        mGamePersister.registerPersister(mWaveManager);

        mGamePersister.registerPersister(new BasicPlateau.Persister(mGameEngine, mEntityRegistry));

        mGamePersister.registerPersister(new Blob.Persister(mGameEngine, mEntityRegistry));
        mGamePersister.registerPersister(new Flyer.Persister(mGameEngine, mEntityRegistry));
        mGamePersister.registerPersister(new Healer.Persister(mGameEngine, mEntityRegistry));
        mGamePersister.registerPersister(new Soldier.Persister(mGameEngine, mEntityRegistry));
        mGamePersister.registerPersister(new Sprinter.Persister(mGameEngine, mEntityRegistry));

        mGamePersister.registerPersister(new Canon.Persister(mGameEngine, mEntityRegistry));
        mGamePersister.registerPersister(new DualCanon.Persister(mGameEngine, mEntityRegistry));
        mGamePersister.registerPersister(new MachineGun.Persister(mGameEngine, mEntityRegistry));
        mGamePersister.registerPersister(new SimpleLaser.Persister(mGameEngine, mEntityRegistry));
        mGamePersister.registerPersister(new BouncingLaser.Persister(mGameEngine, mEntityRegistry));
        mGamePersister.registerPersister(new StraightLaser.Persister(mGameEngine, mEntityRegistry));
        mGamePersister.registerPersister(new Mortar.Persister(mGameEngine, mEntityRegistry));
        mGamePersister.registerPersister(new MineLayer.Persister(mGameEngine, mEntityRegistry));
        mGamePersister.registerPersister(new RocketLauncher.Persister(mGameEngine, mEntityRegistry));
        mGamePersister.registerPersister(new GlueTower.Persister(mGameEngine, mEntityRegistry));
        mGamePersister.registerPersister(new GlueGun.Persister(mGameEngine, mEntityRegistry));
        mGamePersister.registerPersister(new Teleporter.Persister(mGameEngine, mEntityRegistry));
    }

    public ThemeManager getThemeManager() {
        return mThemeManager;
    }

    public Viewport getViewport() {
        return mViewport;
    }

    public Renderer getRenderer() {
        return mRenderer;
    }

    public GameEngine getGameEngine() {
        return mGameEngine;
    }

    public ScoreBoard getScoreBoard() {
        return mScoreBoard;
    }

    public TowerSelector getTowerSelector() {
        return mTowerSelector;
    }

    public TowerControl getTowerControl() {
        return mTowerControl;
    }

    public TowerInserter getTowerInserter() {
        return mTowerInserter;
    }

    public GameConfigurationLoader getGameConfigurationLoader() {
        return mGameConfigurationLoader;
    }

    public WaveManager getWaveManager() {
        return mWaveManager;
    }

    public GameSpeed getSpeedManager() {
        return mSpeedManager;
    }

    public GameState getGameState() {
        return mGameState;
    }

    public SettingsManager getSettingsManager() {
        return mSettingsManager;
    }

    public MapRepository getMapRepository() {
        return mMapRepository;
    }

    public Highscores getHighScores() {
        return mHighScores;
    }
}
