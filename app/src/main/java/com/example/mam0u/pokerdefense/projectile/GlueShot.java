package com.example.mam0u.pokerdefense.projectile;


import com.example.mam0u.pokerdefense.R;
import com.example.mam0u.pokerdefense.effect.GlueEffect;
import com.example.mam0u.pokerdefense.engine.GameEngine;
import com.example.mam0u.pokerdefense.entity.Entity;
import com.example.mam0u.pokerdefense.math.Vector2;
import com.example.mam0u.pokerdefense.sound.Sound;
import com.example.mam0u.pokerdefense.sprite.AnimatedSprite;
import com.example.mam0u.pokerdefense.sprite.SpriteInstance;
import com.example.mam0u.pokerdefense.sprite.SpriteTemplate;
import com.example.mam0u.pokerdefense.sprite.SpriteTransformation;
import com.example.mam0u.pokerdefense.sprite.SpriteTransformer;
import com.example.mam0u.pokerdefense.view.Layers;

public class GlueShot extends Shot implements SpriteTransformation {

    public final static float MOVEMENT_SPEED = 4.0f;
    private final static float ANIMATION_SPEED = 1.0f;

    private class StaticData {

        public SpriteTemplate mSpriteTemplate;
    }
    private float mIntensity;

    private float mDuration;
    private Vector2 mTarget;
    private AnimatedSprite mSprite;

    private Sound mSound;
    public GlueShot(Entity origin, Vector2 position, Vector2 target, float intensity, float duration) {
        super(origin);
        setPosition(position);
        mTarget = target;

        setSpeed(MOVEMENT_SPEED);
        setDirection(getDirectionTo(target));

        mIntensity = intensity;
        mDuration = duration;

        StaticData s = (StaticData) getStaticData();

        mSprite = getSpriteFactory().createAnimated(Layers.SHOT, s.mSpriteTemplate);
        mSprite.setListener(this);
        mSprite.setSequenceForward();
        mSprite.setFrequency(ANIMATION_SPEED);

        mSound = getSoundFactory().createSound(R.raw.gas1_pff);
    }

    @Override
    public Object initStatic() {
        StaticData s = new StaticData();

        s.mSpriteTemplate = getSpriteFactory().createTemplate(R.attr.glueShot, 6);
        s.mSpriteTemplate.setMatrix(0.33f, 0.33f, null, null);

        return s;
    }

    @Override
    public void init() {
        super.init();

        getGameEngine().add(mSprite);
    }

    @Override
    public void clean() {
        super.clean();

        getGameEngine().remove(mSprite);
    }

    @Override
    public void tick() {
        super.tick();

        mSprite.tick();

        if (getDistanceTo(mTarget) < getSpeed() / GameEngine.TARGET_FRAME_RATE) {
            getGameEngine().add(new GlueEffect(getOrigin(), mTarget, mIntensity, mDuration));
            mSound.play();
            this.remove();
        }
    }

    @Override
    public void draw(SpriteInstance sprite, SpriteTransformer transformer) {
        transformer.translate(getPosition());
    }
}
