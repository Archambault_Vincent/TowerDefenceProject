package com.example.mam0u.pokerdefense.game;


public interface GameSpeedListener {
    void gameSpeedChanged();
}
