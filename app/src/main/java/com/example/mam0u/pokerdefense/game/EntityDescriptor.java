package com.example.mam0u.pokerdefense.game;

import com.example.mam0u.pokerdefense.math.Vector2;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;


@Root
public class EntityDescriptor {
    @Element(name = "id")
    private int mId;

    @Element(name = "entityName")
    private String mName;

    @Element(name = "position")
    private Vector2 mPosition;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Vector2 getPosition() {
        return mPosition;
    }

    public void setPosition(Vector2 position) {
        mPosition = position;
    }
}
