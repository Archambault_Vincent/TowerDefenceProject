package com.example.mam0u.pokerdefense.sprite;

public class ReplicatedSprite extends SpriteInstance {
    private final SpriteInstance mOriginal;

    ReplicatedSprite(SpriteInstance original) {
        super(original.getLayer(), original.getTemplate());
        mOriginal = original;
    }

    @Override
    int getIndex() {
        return mOriginal.getIndex();
    }
}
