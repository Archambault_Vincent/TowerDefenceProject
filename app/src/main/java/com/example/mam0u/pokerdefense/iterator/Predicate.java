package com.example.mam0u.pokerdefense.iterator;

public interface Predicate<T> {
    boolean apply(T value);
}
