package com.example.mam0u.pokerdefense.engine;

import com.example.mam0u.pokerdefense.map.MapDescriptorRoot;
import com.example.mam0u.pokerdefense.setting.EnemySettingsRoot;
import com.example.mam0u.pokerdefense.setting.GameSettingsRoot;
import com.example.mam0u.pokerdefense.setting.TowerSettingsRoot;
import com.example.mam0u.pokerdefense.wave.WaveDescriptorRoot;

public class GameConfiguration {
    private final GameSettingsRoot mGameSettings;
    private final EnemySettingsRoot mEnemySettings;
    private final TowerSettingsRoot mTowerSettings;
    private final MapDescriptorRoot mMapDescriptor;
    private final WaveDescriptorRoot mWaveDescriptor;

    public GameConfiguration(GameSettingsRoot gameSettings, EnemySettingsRoot enemySettings,
                             TowerSettingsRoot towerSettings, MapDescriptorRoot mapDescriptor,
                             WaveDescriptorRoot waveDescriptor) {
        mGameSettings = gameSettings;
        mEnemySettings = enemySettings;
        mTowerSettings = towerSettings;
        mMapDescriptor = mapDescriptor;
        mWaveDescriptor = waveDescriptor;
    }

    public GameSettingsRoot getGameSettingsRoot() {
        return mGameSettings;
    }

    public EnemySettingsRoot getEnemySettingsRoot() {
        return mEnemySettings;
    }

    public TowerSettingsRoot getTowerSettingsRoot() {
        return mTowerSettings;
    }

    public MapDescriptorRoot getMapDescriptorRoot() {
        return mMapDescriptor;
    }

    public WaveDescriptorRoot getWaveDescriptorRoot() {
        return mWaveDescriptor;
    }
}
