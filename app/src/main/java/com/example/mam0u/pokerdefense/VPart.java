package com.example.mam0u.pokerdefense;

import android.app.Fragment;
import android.view.View;

import com.example.mam0u.pokerdefense.builder.Factory;
import com.example.mam0u.pokerdefense.setting.SettingsManager;



public class VPart extends Fragment {
    private final SettingsManager mSettingsManager;

    public VPart() {
        Factory factory = Application.getInstance().getGameFactory();
        mSettingsManager = factory.getSettingsManager();
    }

    protected void updateMenuTransparency() {
        View view = getView();

        if (view != null) {
            if (mSettingsManager.isTransparentMenusEnabled()) {
                view.setAlpha(0.73f);
            } else {
                view.setAlpha(1.0f);
            }
        }
    }
}
