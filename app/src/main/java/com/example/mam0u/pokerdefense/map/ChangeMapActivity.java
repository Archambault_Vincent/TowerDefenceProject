package com.example.mam0u.pokerdefense.map;

import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.mam0u.pokerdefense.Application;
import com.example.mam0u.pokerdefense.R;
import com.example.mam0u.pokerdefense.VActivity;
import com.example.mam0u.pokerdefense.builder.Factory;
import com.example.mam0u.pokerdefense.game.GameConfigurationLoader;
import com.example.mam0u.pokerdefense.game.Highscores;
import com.example.mam0u.pokerdefense.theme.ActivityType;

public class ChangeMapActivity extends VActivity implements AdapterView.OnItemClickListener, ViewTreeObserver.OnScrollChangedListener {
    private final GameConfigurationLoader mGameConfigurationLoader;
    private final MapRepository mMapRepository;
    private final Highscores mHighScores;

    private MapsAdapter mAdapter;

    private ImageView arrow_up;
    private ImageView arrow_down;
    private GridView grid_maps;

    public ChangeMapActivity() {
        Factory factory = Application.getInstance().getGameFactory();
        mGameConfigurationLoader = factory.getGameConfigurationLoader();
        mMapRepository = factory.getMapRepository();
        mHighScores = factory.getHighScores();
    }

    @Override
    protected ActivityType getActivityType() {
        return ActivityType.Normal;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_map);

        mAdapter = new MapsAdapter(this, mMapRepository, mHighScores);

        arrow_up = (ImageView) findViewById(R.id.arrow_up);
        arrow_down = (ImageView) findViewById(R.id.arrow_down);

        grid_maps = (GridView) findViewById(R.id.grid_maps);
        grid_maps.setOnItemClickListener(this);
        grid_maps.getViewTreeObserver().addOnScrollChangedListener(this);
        grid_maps.post(new Runnable() {
            @Override
            public void run() {
                updateArrowVisibility();
            }
        });
        grid_maps.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mGameConfigurationLoader.loadMap(mMapRepository.getMapInfos().get(position));
        finish();
    }

    @Override
    public void onScrollChanged() {
        updateArrowVisibility();
    }

    private void updateArrowVisibility() {
        if (grid_maps.getChildCount() <= 0) {
            arrow_up.setVisibility(View.INVISIBLE);
            arrow_down.setVisibility(View.INVISIBLE);
            return;
        }

        if (grid_maps.getFirstVisiblePosition() == 0) {
            arrow_up.setVisibility(grid_maps.getChildAt(0).getTop() < -10 ? View.VISIBLE : View.INVISIBLE);
        } else {
            arrow_up.setVisibility(grid_maps.getFirstVisiblePosition() > 0 ? View.VISIBLE : View.INVISIBLE);
        }

        if (grid_maps.getLastVisiblePosition() == mAdapter.getCount() - 1) {
            arrow_down.setVisibility(grid_maps.getChildAt(grid_maps.getChildCount() - 1).getBottom() > grid_maps.getHeight() + 10 ? View.VISIBLE : View.INVISIBLE);
        } else {
            arrow_down.setVisibility(grid_maps.getLastVisiblePosition() < mAdapter.getCount() - 1 ? View.VISIBLE : View.INVISIBLE);
        }
    }
}
