package com.example.mam0u.pokerdefense.tower;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.mam0u.pokerdefense.R;
import com.example.mam0u.pokerdefense.theme.Theme;
import com.example.mam0u.pokerdefense.view.Drawable;
import com.example.mam0u.pokerdefense.view.Layers;

public class RangeIndicator implements Drawable {
    private final Tower mTower;
    private final Paint mPen;

    public RangeIndicator(Theme theme, Tower tower) {
        mTower = tower;
        mPen = new Paint();
        mPen.setStyle(Paint.Style.STROKE);
        mPen.setStrokeWidth(0.05f);
        mPen.setColor(theme.getColor(R.attr.rangeIndicatorColor));
    }

    @Override
    public int getLayer() {
        return Layers.TOWER_RANGE;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawCircle(mTower.getPosition().x(), mTower.getPosition().y(), mTower.getRange(), mPen);
    }
}
