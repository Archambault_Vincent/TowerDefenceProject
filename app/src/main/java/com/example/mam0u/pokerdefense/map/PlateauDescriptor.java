package com.example.mam0u.pokerdefense.map;

import com.example.mam0u.pokerdefense.math.Vector2;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root
public class PlateauDescriptor {
    @Attribute(name = "name")
    private String mName;

    @Attribute(name = "x")
    private float mX;

    @Attribute(name = "y")
    private float mY;

    public String getName() {
        return mName;
    }

    public Vector2 getPosition() {
        return new Vector2(mX, mY);
    }
}
