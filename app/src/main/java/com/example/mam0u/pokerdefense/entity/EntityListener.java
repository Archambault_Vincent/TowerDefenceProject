package com.example.mam0u.pokerdefense.entity;


public interface EntityListener {
    void entityRemoved(Entity entity);
}
