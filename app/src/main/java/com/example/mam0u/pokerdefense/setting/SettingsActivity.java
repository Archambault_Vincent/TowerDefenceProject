package com.example.mam0u.pokerdefense.setting;

import android.os.Bundle;

import com.example.mam0u.pokerdefense.VActivity;
import com.example.mam0u.pokerdefense.theme.ActivityType;

public class SettingsActivity extends VActivity {
    @Override
    protected ActivityType getActivityType() {
        return ActivityType.Normal;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
}
